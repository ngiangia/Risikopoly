#include "risikopoly.h"
#include <queue>
#include <sys/ioctl.h>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_GIF_Image.H>
#include <FL/fl_ask.H>


#ifndef RISIKOPOLY_HOST_H
#define RISIKOPOLY_HOST_H

class risikopoly_host: private risikopoly
{
	public:
	//************************************* Graphics signals and functions
	Fl_Double_Window* starting_screen;
	Fl_JPEG_Image *bg_image; 
	Fl_Box* bg_box; 
	Fl_Button* load_btn;
	Fl_Button* new_game_btn;
	Fl_Button* start_btn;
	Fl_Menu_Bar* menu_bar;
	Fl_Text_Display* display;
	Fl_Text_Buffer *text_buff;

	void draw_starting_screen();

	//************************************* TCIP signals and functions
	int host_port, host_sockfd[7], client_sockfd;
	string message_received;
	bool start_host(int N);
	int send_message(int N, string message);
	int receive_message(string message_to_send);
	bool wait_for_connections();

	//************************************* GAMEPLAY signals and functions
	queue <string> pending_messages[6];
	bool setup_game();
	void load_game();
	int game();

	risikopoly_host();
	~risikopoly_host();
};

#endif
