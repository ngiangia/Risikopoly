#include "risikopoly_host.h"
#include <algorithm>
#include <vector> 

Fl_Menu_Item menu_[] = 
{
	{"File", 0,  0, 0, 192, FL_ENGRAVED_LABEL, 0, 14, 0},
	{"Save", 0,  0, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
	{"item", 0,  0, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
	{0,0,0,0,0,0,0,0,0},
	{"Edit", 0,  0, 0, 192, FL_ENGRAVED_LABEL, 0, 14, 0},
	{"What do you want to edit?\?\?", 0,  0, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
	{0,0,0,0,0,0,0,0,0},
	{"Help", 0,  0, 0, 192, FL_ENGRAVED_LABEL, 0, 14, 0},
	{"Did you really press help?\?\?", 0,  0, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
	{0,0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,0,0,0}
};

risikopoly_host::risikopoly_host()
{
	
}

// This window callback allows the user to save & exit, don't save, or cancel.
static void window_cb (Fl_Widget *widget, void *) 
{
	Fl_Window *window = (Fl_Window *)widget;
	fl_message_title("Nooooooooooo =(");
	int result = fl_choice("Do you want to save before quitting?", 
		"Don't Save",  // 0
		"Save",        // 1
		"Cancel"       // 2
	);
	if (result == 0) 
	{  // Close without saving
		window->hide();
		throw(1);
	} 
	else if (result == 1) 
	{  // Save and close
		//save();
		window->hide();
		throw(0);
	} 
	else if (result == 2) 
	{  // Cancel / don't close
	
		// don't do anything
	}
}


risikopoly_host::~risikopoly_host()
{
	for(int i = 0; i<n_players; i++)	
	{
		close(host_sockfd[i]);		
	}
	close(client_sockfd);
}

void risikopoly_host::draw_starting_screen()
{
	starting_screen 	= new Fl_Double_Window(1200, 705);
	bg_image 			= new Fl_JPEG_Image("Images//starting_screen.jpg");
	bg_box 			= new Fl_Box(0, 20, 1200, 700);  
	load_btn 			= new Fl_Button(100, 360, 260, 75, "LOAD");
	new_game_btn 		= new Fl_Button(100, 255, 260, 75, "NEW GAME");
	start_btn 		= new Fl_Button(465, 365, 215, 55, "START");
	menu_bar 			= new Fl_Menu_Bar(0, 0, 1307, 30);
	display 			= new Fl_Text_Display(25, 200, 385, 450);
	text_buff 		= new Fl_Text_Buffer();

	starting_screen->callback(window_cb);

	bg_box->image(bg_image);

	new_game_btn->box(FL_ENGRAVED_BOX);
	new_game_btn->color((Fl_Color)3);
	new_game_btn->labeltype(FL_SHADOW_LABEL);
	new_game_btn->labelfont(14);
	new_game_btn->labelsize(25); 
	
	load_btn->box(FL_ENGRAVED_BOX);
	load_btn->color((Fl_Color)3);
	load_btn->labeltype(FL_SHADOW_LABEL);
	load_btn->labelfont(14);
	load_btn->labelsize(25);

	start_btn->box(FL_ENGRAVED_BOX);
	start_btn->color((Fl_Color)3);
	start_btn->labeltype(FL_SHADOW_LABEL);
	start_btn->labelfont(14);
	start_btn->labelsize(25);
	start_btn->hide();
	start_btn->deactivate();

	menu_bar->menu(menu_);

	display->box(FL_ROUNDED_FRAME);
	display->color(FL_FOREGROUND_COLOR);
	display->labeltype(FL_NO_LABEL);
	display->labelfont(13);
	display->textfont(13);
	display->textcolor(FL_BACKGROUND2_COLOR);
	display->buffer(text_buff);
	display->hide();

	starting_screen->end();
	starting_screen->show();

	while(Fl::wait())
	{	
		if(new_game_btn->changed())
		{
			new_game_btn->clear();
			new_game_btn->hide();
			new_game_btn->deactivate();

			start_btn->show();
			start_btn->activate();

			load_btn->hide();
			load_btn->deactivate();

			display->show();

			wait_for_connections();	
			setup_game();
			break;
		}

		if(load_btn->changed())
		{
			new_game_btn->hide();
			new_game_btn->deactivate();

			start_btn->show();
			start_btn->activate();

			load_btn->clear();
			load_btn->hide();
			load_btn->deactivate();

			display->show();

			wait_for_connections();	
			load_game();
			break;
		}
	}	
	
}







void error(const char *msg)
{
	perror(msg);
	exit(1);
}

bool risikopoly_host::start_host(int N)
{	
	cout << "Starting host, port "<<5000+N<<endl;
	host_sockfd[N] = socket(AF_INET, SOCK_STREAM, 0);
	if (host_sockfd[N] < 0) 
	{
		//allegro_message("ERROR opening socket");
		return false;
	}
	int reuse = 1;
	if (setsockopt(host_sockfd[N], SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse)) < 0)
	{
   		 //allegro_message("setsockopt(SO_REUSEADDR) failed");
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(5000+N);
	clilen = sizeof(cli_addr);
	if (bind(host_sockfd[N], (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)   
	{
		//allegro_message("ERROR on binding");
		return false;
	}
	u_long nbio = 1;
	ioctl(host_sockfd[N], FIONBIO, &nbio);
	listen(host_sockfd[N],1);
	return true;
}

int risikopoly_host::send_message(int N, string message)
{				
	client_sockfd = accept(host_sockfd[N], (struct sockaddr *) &cli_addr, &clilen);
	if (client_sockfd >= 0) 
	{
		int n = write(client_sockfd, message.c_str(), message.length());
		if (n < 0)
		{
			fl_message("ERROR writing to socket");
			cout << "ERROR writing to socket\n";
			return -1;
		} 
		else
		{
			int n = read(client_sockfd,sock_buffer,255);
			if (n < 0) 
			{
				fl_message("ERROR reading from socket");
				cout << "ERROR reading from socket\n";
				return -2;
			}
			else
			{
				message_received = sock_buffer;
				close(client_sockfd);
				return 1;
			}
		}
	}
	else
	{
		close(client_sockfd);
		return 0;
	}
}

int risikopoly_host::receive_message(string message_to_send = "Test message")
{	
	int return_value = 0;
	message_received = "";	
	client_sockfd = accept(host_sockfd[0], (struct sockaddr *) &cli_addr, &clilen);
	if (client_sockfd >= 0) 
	{
		bzero(sock_buffer,256);
		int n = read(client_sockfd,sock_buffer,255);
		if (n < 0) 
		{
			//allegro_message("ERROR reading from socket");
			return -1;
		}
		else
		{
			message_received = sock_buffer;
			if(message_received.find(",") != string::npos)
			{		
				int pos = message_received.find(",");
				message_to_send = "";
				string temp = message_received;
				int instruction = atoi(temp.substr(0, pos).c_str());
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				return_value = atoi(temp.substr(0, pos).c_str()) + 1;
				if(instruction != SYNC)
				{
					for(int i=0; i<n_players; i++)
					{
						pending_messages[i].push(message_received);
					}
					stringstream out_message;
					out_message << "Received message \"" << message_received << "\" from " << gamer[return_value - 1].name << endl; 
					text_buff->insert(0, out_message.str().c_str());
					cout << "Message sender: "<< gamer[return_value-1].name << "; instruction number: " << instruction << endl;
				}
				stringstream N_of_pending_messages;
				N_of_pending_messages << pending_messages[return_value - 1].size();
				message_to_send = N_of_pending_messages.str();
			}
			else
			{
				return_value = 10;
			}
			//cout << message_received << endl;
			n = write(client_sockfd, message_to_send.c_str(), message_to_send.length());
			if (n < 0)
			{
				fl_message("ERROR writing to socket");
				cout << "ERROR writing to socket\n";
				return -2;
			} 
			else
			{
				/*if (N == 0)
				{
					close(client_sockfd);
					return 10;
				}*/
				close(client_sockfd);
				return return_value;
			}
		}
	}
	else
	{
		close(client_sockfd);
		return 0;
	}

}




bool risikopoly_host::wait_for_connections()
{
	int M;
	stringstream message, text;
	text  << "Players connected: "<< n_players << endl;
	text_buff->text(text.str().c_str());
	Fl::flush();
	while(1)
	{
		Fl::wait(0.1);
		message.str("");
		message << n_players + 1;
		M = receive_message(message.str());
		if(M < 0)
		{
			return false;
		}
		else if(M == 10)
		{
			if(message_received.find("Name:") != string::npos)
			{	
				message.str("");
				close(client_sockfd);
				gamer[n_players].name = new char[message_received.length() -5];
				strcpy(gamer[n_players].name, message_received.substr(6).c_str());
				fl_message("Player %s connected", gamer[n_players].name);
				n_players++;
				text.str("");
				text  << "Players connected: "<< n_players << endl;
				for(int i =0; i < n_players; i++)
				{
					text << "Player "<< i+1 << ": "<<gamer[i].name << endl;
				}
				text_buff->text(text.str().c_str()); 
			}
			
		}
		if(start_btn->changed())
		{
			text.str("");
			text  << "Setting Game\n";
			start_btn->clear();
			start_btn->hide();
			break;
		}
	}
	return true;
}


bool risikopoly_host::setup_game()
{
	int n_objects[50];
	stringstream message;
	
	//draw_sprite(buf, background_screen, 0, 0);
	//doppiobuffering();

	for(int i=0; i<50; i++)
	{
		n_objects[i]=i;
	}

	random_shuffle(&n_objects[0],&n_objects[N_CONTRACTS]);	//shuffle contracts 
	for(int N =0; N<n_players; N++) //loop over players
	{
		for(int i=0; i<N_CONTRACTS; i++) 		//assign contracts to players
		{
			if(i >= n_contracts_per_player[n_players-1]*N && i < n_contracts_per_player[n_players-1]*(N+1))
			{
				contr[n_objects[i]].owner = N;
			}
			
		}
		
	}

	random_shuffle(&n_objects[0],&n_objects[N_TERRITORIES]);	//shuffle territories
	int terr_per_player = N_TERRITORIES/n_players;
	for(int N = 0; N<n_players; N++)
	{
		for(int i=0; i<N_TERRITORIES; i++) 		//assign territories to players
		{
			if(i>= terr_per_player*N && i < terr_per_player*(N+1))
			{
				terr[n_objects[i]].owner = N;
			}
		}
	}

	//send init info
	message <<INIT_INFO << "," << n_players;
	for (int N=0 ; N< n_players; N++)
	{
		//start_host(N+1);
		message << ","<< gamer[N].name;
	}
	cout << "Sending message:\t\t"<<message.str()<<endl;
	for(int n=0; n<n_players; n++)
	{
		while(1)
		{
			if(send_message(n + 1, message.str()) == 1)
			{	
				break;
			}
		}
	}
	cout << "Done\n";
	message.str("");

	//send all_contracts info
	message << ALL_CONTRACTS;
	for(int i=0; i<N_CONTRACTS; i++)
	{
		message << "," << contr[i].owner;
	}
	cout << "Sending message:\t\t"<<message.str()<<endl;
	for(int n=0; n<n_players; n++)
	{
		while(1)
		{
			if(send_message(n + 1, message.str()) == 1)
			{	
				break;
			}
		}
	}
	cout << "Done\n";
	message.str("");

	//send all_territories info
	message << ALL_TERRITORIES;
	for(int i=0; i<N_TERRITORIES; i++)
	{
		message << "," << terr[i].owner;
	}
	cout << "Sending message:\t\t"<<message.str()<<endl;
	for(int n=0; n<n_players; n++)
	{
		while(1)
		{
			if(send_message(n + 1, message.str()) == 1)
			{	
				break;
			}
		}
	}
	cout << "Done\n";
	message.str("");

	//send territory cards info
	message << ALLTERRCARDS;
	for(int i=0; i<50; i++)
	{
		n_objects[i]=i;
	}
  	random_shuffle(&n_objects[0],&n_objects[(N_TERRITORY_CARDS)]);	//shuffle territories cards
	for(int i=0; i<N_TERRITORY_CARDS; i++)
	{
		message << "," << n_objects[i];
	}
	cout << "Sending message:\t\t"<<message.str()<<endl;
	for(int n=0; n<n_players; n++)
	{
		while(1)
		{
			if(send_message(n + 1, message.str()) == 1)
			{	
				break;
			}
		}
	}
	cout << "Done\n";
	message.str("");

	//send chance cards info
	message << ALLCHANCECARDS;
	for(int i=0; i<50; i++)
	{
		n_objects[i]=i;
	}
	random_shuffle(&n_objects[0],&n_objects[CHANCE_CARDS]);	//shuffle chance card
	for(int i=0; i<CHANCE_CARDS; i++)
	{
		message << "," << n_objects[i];
	}
	cout << "Sending message:\t\t"<<message.str()<<endl;
	for(int n=0; n<n_players; n++)
	{
		while(1)
		{
			if(send_message(n + 1, message.str()) == 1)
			{	
				break;
			}
		}
	}
	cout << "Done\n";
	message.str("");

	//send chest cards info
	message << ALLCHESTCARDS;
	for(int i=0; i<50; i++)
	{
		n_objects[i]=i;
	}
	random_shuffle(&n_objects[0],&n_objects[CHEST_CARDS]);	//shuffle chest card
	for(int i=0; i<CHEST_CARDS; i++)
	{
		message << "," << n_objects[i];
	}
	cout << "Sending message:\t\t"<<message.str()<<endl;
	for(int n=0; n<n_players; n++)
	{
		while(1)
		{
			if(send_message(n + 1, message.str()) == 1)
			{	
				break;
			}
		}
	}
	cout << "Done\n";
	message.str("");


}

void risikopoly_host::load_game()
{	
	stringstream message;
	message.str("");
	message << LOAD;
	cout << "Sending message:\t\t"<<message.str()<<endl;
	for(int n=0; n<n_players; n++)
	{
		while(1)
		{
			if(send_message(n + 1, message.str()) == 1)
			{	
				break;
			}
		}
	}
	cout << "Done\n";
	for(int i=0; i<5; i++)
	{
		message.str("");	
		message << NOTHING << ",";
		cout << "Sending message:\t\t"<<message.str()<<endl;
		for(int n=0; n<n_players; n++)
		{
			while(1)
			{
				if(send_message(n + 1, message.str()) == 1)
				{	
					break;
				}
			}
		}
		cout << "Done\n";
		message.str("");
	}
}

int risikopoly_host::game()
{
	int sender;
	int N_message = 0, N_messages;
	stringstream out_message;
	out_message.str("");
	while(1)
	{
		Fl::wait(0.1);
		if((sender = receive_message()) > 0)
		{	
			N_message = 0;
			N_messages = pending_messages[sender - 1].size();
			while(pending_messages[sender - 1].size()>0)
			{
				cout << "Sending message " << (N_messages - pending_messages[sender - 1].size() + 1) << " out of " << N_messages << " to " << gamer[sender-1].name <<endl; 
				while(1)
				{
					if(send_message(sender, pending_messages[sender - 1].front()) == 1)
					{	
						pending_messages[sender - 1].pop();
						N_message ++;
						break;
					}
				}
				cout << "Done\n";
			}
		}
	}


}
