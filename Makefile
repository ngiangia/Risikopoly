CC=g++
CFLAGS=-I $(INCDIR) -W  -std=gnu++11 -pthread
LDFLAGS=`allegro-config --libs`

COMMON_DIR=common
HOST_DIR=host
CLIENT_DIR=client
COMMON_SRCDIR=$(COMMON_DIR)/src
COMMON_INCDIR=$(COMMON_DIR)/inc
HOST_SRCDIR=$(HOST_DIR)/src
HOST_INCDIR=$(HOST_DIR)/inc
CLIENT_SRCDIR=$(CLIENT_DIR)/src
CLIENT_INCDIR=$(CLIENT_DIR)/inc
BINDIR=bin
OBJDIR=obj

COMMON_INCFILES= $(shell find $(COMMON_INCDIR) -name \*.h)
COMMON_SOURCES=  $(shell find $(COMMON_SRCDIR) -name \*.cpp)
HOST_INCFILES=   $(shell find $(HOST_INCDIR) -name \*.h)
HOST_SOURCES=    $(shell find $(HOST_SRCDIR) -name \*.cpp)
CLIENT_INCFILES= $(shell find $(CLIENT_INCDIR) -name \*.h)
CLIENT_SOURCES=  $(shell find $(CLIENT_SRCDIR) -name \*.cpp)


.PHONY: clean host client

all: host client

clean:
	@rm -f $(OBJDIR)/*.o $(BINDIR)/* $(COMMON_DIR)/*~ $(COMMON_SRCDIR)/*~ $(COMMON_INCDIR)/*~ $(HOST_DIR)/*~ $(HOST_INCDIR)/*~ $(HOST_SRCDIR)/*~ $(CLIENT_DIR)/*~ $(CLIENT_SRCDIR)/*~ $(CLIENT_INCDIR)/*~ ./*~
	@echo "All clean"

host:$(COMMON_INCFILES) $(COMMON_SOURCES) $(HOST_INCFILES) $(HOST_SOURCES)
	@make -C $(HOST_DIR)/

client:$(COMMON_INCFILES) $(COMMON_SOURCES) $(CLIENT_INCFILES) $(CLIENT_SOURCES)
	@make -C $(CLIENT_DIR)/
	
