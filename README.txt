################################################
#                                              #
#              RISIKOPOLY V1.0                 #
#          created by Nico Giangiacomi         #
#                                              #
################################################


Packages required:

1) FLTK 	-> Download from "http://www.fltk.org/software.php"
		-> (Linux) Follow README for installing
		-> (Windows) Follow instructions later for installing
2) ALLEGRO 	-> Dowload version 4.4.2 from "http://liballeg.org/old.html"
		-> (Linux) Follow instructions for installing
		-> (Windows) Follow instructions later for installing
3) CYGWIN (only WINDOWS) -> Download from http://www.cygwin.com/install.html
		-> run setup-x86.exe and follow instructions
		-> add following packages: (search and left-click on "skip")
					-git (Distributed version control system)
					-make (The GNU version of the 'make' utility)
					-cmake (Cross-platform makefile generation system)
					-gcc-g++ (GNU Compiler Collection)
					-xorg-server (X Org X servers)
					-xinit (X Org X server launcher)
					-libX11-devel (X Org X11 core library)

################# Ubuntu #####################
Building:
run (from command line): 
	$ sudo apt-get install libxcursor-dev
	$ sudo apt-get install libxinerama-dev
	$ cd path-to-Risikopoly-download-place
	$ mkdir obj && mkdir bin && mkdir savings
	$ make

Play:
run (from command line): 
	$ cd path-to-Risikopoly-download-place
	$ bin/Risikopoly
	
################# Windows #####################
Building:
Open CygWin64 Terminal
	run: 
		$ cd C:
		$ cd path-to-fltk-download-place
		$ ./configure  --enable-cygwin
		$ ./cmake .
		$ make
		$ make install 
		
		$ cd path-to-allegro-download-place
		$ ./cmake .
		$ make
		$ make install 	
		
		$ cd path-to-Risikopoly-download-place
		$ mkdir obj && mkdir bin && mkdir savings	
		$ make
		$ make install 
		
Play:
Open CygWin64 Terminal
	run:
		$ cd C:
		$ cd path-to-Risikopoly-download-place
		$ startxwin&
		$ export DISPLAY=:0.0
		$ bin/Risikopoly.exe
