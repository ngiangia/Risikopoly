#include "allegro.h"
#include "risikopoly.h"
#include <sys/ioctl.h>
#include <queue>
#include <ctime>
#include <chrono>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_GIF_Image.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Check_Button.H>
#include "info.h"
#include <stdlib.h>
#include <fstream>

#ifndef RISIKOPOLY_CLIENT_H
#define RISIKOPOLY_CLIENT_H

class risikopoly_client: private risikopoly
{
	//************************************* Graphics signals and functions
	Fl_Double_Window* main_screen;
	Fl_JPEG_Image *bg_main_image, *bg_second_image;
	Fl_Box* bg_main_box, *bg_second_box;
	Fl_GIF_Image *territory_image[N_TERRITORIES];
	Fl_Box* territory_box[N_TERRITORIES];	
	Fl_GIF_Image *dice_image[6], *pawn_image[6];
	Fl_Box* dice_box[6], *pawn_box[6];
	Fl_Menu_Bar* menu_bar;
	Fl_Text_Display* main_display;
	stringstream main_text;
	Fl_Text_Buffer *main_text_buff;
	Fl_Text_Display* info_display;
	Fl_Text_Buffer *info_text_buff;
	Fl_Text_Display* army_display;
	Fl_Text_Buffer* army_text_buff;
	stringstream info_text;
	Fl_Button* offer_btn;
	Fl_Button* not_interested_btn;
	Fl_Button* yes_btn;
	Fl_Button* no_btn;
	Fl_Button* ok_btn;
	Fl_Button* change_view_btn;
	Fl_Button* state_btn[45];
	Fl_Button* exchange_btn;
	Fl_Float_Input* offer_input;
	Fl_Check_Button* audio_select;
	Fl_Button* contract_btn[N_CONTRACTS];
	Fl_Box* house_box[(N_CONTRACTS-6)*5];
	Fl_Button* terr_card_btn[6];
	Fl_Button* pri_exit_btn[12];

	SAMPLE *intro_theme, *main_theme;

	int window;		//0 -> board window, 1 -> contracts window

	void graphics_init();
	void draw_starting_screen();
	void decode_territory_name(int N_terrytory, int color);
	void decode_pawn_name(short int Player);
	void decode_contract_btn(int contract_number);
	void decode_terr_card_btn();
	void decode_pri_exit_btn();
	void check_buttons();
	void draw_dice(short int die1 = 0, short int die2 = 0);
	void draw_houses();

	//************************************* TCIP signals and functions
	int client_sockfd;
	int host_port;
	string message_received;
	queue <string> list_of_messages;
	string host_ip;	
	chrono::high_resolution_clock::time_point last_sync;
	int n_of_pending_messages;

	int send_message(string message);
	int receive_message(string message_to_send = "Test message");
	int update_messages();
	int synchronize();
	int send_all_messages();

	//************************************* GAMEPLAY signals and functions
	bool new_game, game_loaded;
	short int selected_state, attacker, attacked, armies_atk, armies_def, armies_atk_lost;
	short int atk_value[3], def_value[3], yellow_die[3];
	int offered_items[4+N_CONTRACTS], desidered_items[4+N_CONTRACTS];
	short int exchange_offerer, exchange_receiver;
	bool fighting, under_attack;
	bool exchange_happened, from_offer, payment_needed;
	bool first_start_passed[6];
	bool disposing_tanks;
	bool attack_successfull;
	int turnsInPrison[6], numberOfMove;
	queue <int> terr_card_deck, chance_deck, chest_deck;
	int n_cannons[6], n_pedestrians[6], n_horses[6], n_jollies[6];

	void next_turn();
	bool decode_instruction(string instruction);
	int connect_player(string name, string host_address);
	int get_initial_info();
	int auction();
	int get_money_auction();
	int get_new_offer();
	void fight();
	void select_fighting_territories(bool FIGHT = true);	
	void dice_attack();
	void conquer_territory();
	void movement();
	void proceed_destination(short int Player);
	string make_offer(short int offerer, short int receiver, short int INSTR, bool visualize=false);
	short int select_armies(int atk=0);
	short int choose_player();
	void count_player_terr();
	bool contr_series_full(int contr_number);
	bool build_house_ok(int contr_number);
	bool mortgage_ok(int contr_number);
	void update_info_text();
	void house_construction(int contr_number);
	bool check_offer_validity();
	void payment(bool isContract = true, int price = 0, int owner = 100);
	void sell_tanks(int n_terr);
	void buy_tanks();
	void dispose_tanks(int n_tanks);
	void remove_tanks(int n_tanks, bool onlyYou);
	void write_info_message(string message, bool pop);
	void move_tanks();
	void decode_terr_card();
	void terr_card_view(int number);
	int find_init_money(int PLAYER);
	void decode_chance(int Player, int chance_picked);
	void decode_chest(int Player, int chest_picked);
	void process_stations();
	int find_station_player(int Player);
	int max_tank_remove(int Player);

	void save();
	void load();
	void prepare_game();
	public:
	void Close();
	void play_game();
	void pay_from_cb(int amount);

	risikopoly_client();
	~risikopoly_client();
};

#endif
