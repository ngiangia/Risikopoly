#ifndef INFO_H
#define INFO_H

static int contract_position[N_CONTRACTS] 	= {  1,   3,   6,   8,   9,  11,  13,  14,  16,  18,  19,  21,  23,  24,  26,  27,  29,  31,  32,  34,  37,  39,   5,  15,  25,  35,  12,  28};
static int position_contract[40]			= {800,   0, 100,   1, 300,  22,   2, 200,   3,   4, 400,   5,  26,   6,   7,  23,   8, 100,   9,  10, 500,  11, 200,  12,  13,  24,  14,  15, 27, 16, 600,  17, 18, 100, 19, 25, 200, 20, 700, 21};

static int contract_btn_x[N_CONTRACTS] 		= {  5,   5,  43,  43,  43,  81,  81,  81, 117, 117, 117, 150, 150, 150, 185, 185, 185, 218, 218, 218, 250, 250, 285, 285, 285, 285, 320, 320};
static int contract_btn_y[N_CONTRACTS] 		= {143, 173, 143, 173, 202, 143, 173, 202, 143, 173, 202, 143, 173, 202, 143, 173, 202, 143, 173, 202, 143, 173, 143, 173, 202, 230, 143, 173};
static int contract_btn_color[N_CONTRACTS] 	= {168, 168, 180, 180, 180,  92,  92,  92,  74,  74,  74,  88,  88,  88,   3,   3,   3,  61,  61,  61, 112, 112,  18,  18,  18,  18,  17,  17};
static string contract_btn_label[N_CONTRACTS] 	= {"1", "2", "1", "2", "3", "1", "2", "3", "1", "2", "3", "1", "2", "3", "1", "2", "3", "1", "2", "3", "1", "2", "1", "2", "3", "4", "1", "2"};

static int contract_series[N_CONTRACTS]		= {  0,   0,   1,   1,   1,   2,   2,   2,   3,   3,   3,   4,   4,   4,   5,   5,   5,   6,   6,   6,   7,   7,   8,   8,   8,   8,   9,   9};
static int contract_series_full[N_CONTRACTS_SERIES] = {2, 3, 3, 3, 3, 3, 3, 2, 4, 2};

static string contract_name[N_CONTRACTS] 				= {"Vicolo Corto", "Vicolo Stretto", "Bastioni Gran Sasso", "Viale Monterosa", "Viale Vesuvio", "Via Accademia", "Corso Ateneo", "Piazza Universita'", "Via Verdi", "Corso Magellano", "Piazza Dante", "Via Marco Polo", "Corso Magellano", "Largo Colombo", "Viale Costantino", "Viale Traiano", "Piazza Giulio Cesare", "Via Roma", "Corso Impero", "Largo Augusto", "Viale dei Giadini", "Parco della Vittoria", "Stazione Sud", "Stazione Ovest", "Stazione Nord", "Stazione Est", "Societa' Acqua Potabile", "Societa' Elettrica"};
static int contract_price[N_CONTRACTS] 					= {6000, 8000, 10000, 10000, 12000, 14000, 14000, 16000, 18000, 18000, 20000, 22000, 22000, 24000, 26000, 26000, 28000, 30000, 30000, 32000, 35000, 40000, 20000, 20000, 20000, 20000, 15000, 15000};
static int contract_house_price[N_CONTRACTS_SERIES] 	= {5000, 5000, 10000, 10000, 15000, 15000, 20000, 20000, 0, 0};
static int value_house_0[N_CONTRACTS]					= {  200,   400,   600,   600,   800,  1000,  1000,  1200,  1400,  1400,   1600,   1800,   1800,   2000,   2200,   2200,   2400,   2600,   2600,   2800,   3500,   5000,  2500,  2500,  2500,  2500,  2000,  2000};
static int value_house_1[N_CONTRACTS]					= { 1000,  2000,  3000,  3000,  4000,  5000,  5000,  6000,  7000,  7000,   8000,   9000,   9000,  10000,  11000,  11000,  12000,  13000,  13000,  15000,  20000,  20000,  5000,  5000,  5000,  5000, 10000, 10000};
static int value_house_2[N_CONTRACTS]					= { 3000,  6000,  9000,  9000, 10000, 15000, 15000, 18000, 20000, 20000,  22000,  25000,  25000,  30000,  33000,  33000,  36000,  40000,  40000,  45000,  50000,  60000, 10000, 10000, 10000, 10000,     0,     0};
static int value_house_3[N_CONTRACTS]					= { 9000, 18000, 27000, 27000, 30000, 65000, 65000, 50000, 55000, 55000,  60000,  70000,  70000,  75000,  80000,  80000,  85000,  90000,  90000, 100000, 110000, 140000, 20000, 20000, 20000, 20000,     0,     0};
static int value_house_4[N_CONTRACTS]					= {16000, 32000, 40000, 40000, 45000, 42000, 42000, 70000, 75000, 75000,  80000,  88000,  88000,  90000, 100000, 100000, 105000, 110000, 110000, 120000, 130000, 170000,     0,     0,     0,     0,     0,     0};
static int value_house_5[N_CONTRACTS]					= {25000, 45000, 55000, 55000, 60000, 75000, 75000, 90000, 95000, 95000, 100000, 105000, 105000, 110000, 120000, 120000, 125000, 130000, 130000, 140000, 150000, 200000,     0,     0,     0,     0,     0,     0};


static string chance_name[CHANCE_CARDS]					= {"Matrimonio in famiglia, spese impreviste 15000",
															"Versate 2000 lire in beneficienza",
															"Andate sino a Largo Colombo, se passate dal Via ritirate 20000 ctk",
															"Andate fino a Parco della Vittoria",
															"Dovete pagare un contributo di miglioria stradale.\n4000 ctk per ogni casa, 10000 ctk per ogni albergo",
															"Andate sino a Via Accademia, se passate dal Via ritirate 20000 ctk",
															"Avete tutti gli stabili da riparare.\n Pagate 2500 ctk per ogni casa e 10000 ctk per ogni albergo",
															"Multa di 15000 ctk per aver guidato senza patente",
															"Maturano le cedole delle vostre cartelle d rendita.\n Ritirate 15000 ctk",
															"Avete vinto un terno al lotto, ritirate 10000 ctk.",
															"Andate sino alla Stazione Nord, se passate dal Via ritirate 20000 ctk",
															"La Banca vi paga gli interessi del vostro conto corrente.\nRitirate 5000 ctk",
															"Fate tre passi indietro (ma senza i tanti auguri)",
															"Andate avanti sino al Via",
															"Andate in prigione direttamente e senza passare dal Via",
															"Uscite gratis di prigione",
															"Premio Stazioni:\n0 Stazioni = perdi 10 carri armati\n1 Stazione = nessun effetto\n2 Stazioni = vinci 3 carri armati\n3 Stazioni = rimuovi 5 carri armati\n4 Stazioni = vinci 5 carri armati, rimuovi 5 carri armati",
															"Raccogliete 500000 ctk oppure tirate un dado e \nscambiatevi di tanti posti quanti il risultato"
														};

static string chest_name[CHEST_CARDS]					= {"Se possiedi il Giappone, vinci 150000 ctk",
															"Scade il vostro premio di assicurazione, pagate 5000 ctk",
															"Avete perso una causa, pagate 10000 ctk",
															"E' maturata la cedula delle vostre azioni,\n ritirate 2500 ctk",
															"E' il vostro compleanno, ogni giocatore vi regala 10000 ctk",
															"Andate in prigione direttamente e senza passare dal Via",
															"Avete vinto il secondo premio di un concorso di bellezza\nRitirate 5000 ctk",
															"Ereditate da un lontano parente 10000 ctk",
															"Andate fino al Via",
															"Pagate il conto del Dottore, 5000 ctk",
															"Avete vinto il premio di consolazione della lotteria di Merano\nRitirate 10000 ctk",
															"Uscite gratis di prigione",
															"Ritornate a Vicolo Corto",
															"Pagate una multa di 15000 ctk, oppure prendete un cartoncino da Imprevisti",
															"Sei creditore verso la Banca di 20000 ctk, ritirali",
															"Avete venduto delle Azioni, ricavate 5000 ctk",
															"Se possiedi il Giappone, vinci 3 carri armati"
														};

#endif
