#include"risikopoly_client.h"

risikopoly_client::risikopoly_client()
{
	host_port = 5000;
	window = 0;
	new_game = false;
	game_loaded = false;
	current_offer[0] = 0;
	current_offer[1] = 2000;
	selected_state = 50;
	fighting = under_attack = payment_needed = false;
	exchange_happened = false;
	disposing_tanks = false;
	from_offer = false;
	numberOfMove = 0;
	for(int i=0; i<6; i++)
	{
		gamer[i].state = 0;
		turnsInPrison[i] = 0;
	}
	for(int i = 0; i<6; i++)
	{	
		first_start_passed[i] = false;
	}
	attacker = 100;
	attacked = 100;
	for(int i=0; i<N_CONTRACTS; i++)
	{
		contr[i].kind =  contract_series[i];
		contr[i].name =  contract_name[i];
		contr[i].price = contract_price[i];
		contr[i].price_house = contract_house_price[contr[i].kind];
		contr[i].n_houses = 0;
		contr[i].mortgage = false;
		contr[i].owner = 100;
		contr[i].value_house[0] = value_house_0[i];
		contr[i].value_house[1] = value_house_1[i];
		contr[i].value_house[2] = value_house_2[i];
		contr[i].value_house[3] = value_house_3[i];
		contr[i].value_house[4] = value_house_4[i];
		contr[i].value_house[5] = value_house_5[i];
	}
	last_sync =  chrono::high_resolution_clock::now();
	allegro_init();
	if (install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, NULL) != 0) 
	{
		allegro_message("Error initialising sound system\n%s\n", allegro_error);
	}

	graphics_init();
	
}


risikopoly_client::~risikopoly_client()
{
	Fl_Double_Window* starting_screen;
}

void risikopoly_client::Close()
{
	stringstream message;
	message << CLOSE << "," << gamer_ID;
	list_of_messages.push(message.str()); 
	send_all_messages();
}


int risikopoly_client::connect_player(string name, string host_address)
{
	string message="Name: ";
	message.append(name);
	host_ip = host_address.c_str();
	//host_ip = "localhost"; //"192.168.1.37";
	
	int n=1;
	
	while(n)
	{
		n = send_message(message.c_str()); 
		if(n < 0)
		{
			return n;
		}
	}
	gamer_ID =  atoi(message_received.c_str()) - 1;
	host_port = 5000 + gamer_ID + 1;
	return 0;


}

int risikopoly_client::get_initial_info()
{
	int i=0;
	while(i < 6) //receiving 6 istructions from master: init_info, territories, contracts, territory_cards, chance_cards, chest_cards 
	{
		int n=1;
		while(n)
		{
			n = receive_message(); 
			if(n < 0)
			{
				return n;
			}
		}
		if(!decode_instruction(message_received))
		{	
			cout<<"Failed decoding\n";
			return -6;
		}
		else
		{
			cout<<"Decoding ok\n";
			i++;
		}
	}
	if(new_game)
	{
		for(int j = 0; j < n_players; j++)
		{
			gamer[j].money = 400000 - 50000*n_players;
			gamer[j].color = j;
		}
	}
	for(int j = 0; j < N_TERRITORIES; j++)
	{
		if(terr[j].owner != 100)
		{
			decode_territory_name(j, gamer[terr[j].owner].color);
		}
		else
		{
			decode_territory_name(j, EMPTY);
		} 
	}
}

void risikopoly_client::write_info_message(string message, bool pop = false)
{
	main_text.str("");
	main_text << "************************************************\n";
	main_text << message << endl;
	main_text_buff->insert(0, main_text.str().c_str());

	if(pop)
	{
		fl_message("%s", message.c_str());
	}
}

int risikopoly_client::get_new_offer()
{
	int m = receive_message(""); 
	if(m < 0)
	{
		return m;
	}
	else if(m == 0)
	{
		if(!decode_instruction(message_received))
		{	
			//cout<<"Failed decoding\n";
			return -6;
		}	
	}
}

int risikopoly_client::auction()
{
	fl_message("AUCTION!");
	offer_input->show();
	not_interested_btn->show();
	offer_btn->show();
	stringstream message;
	N_offerers = n_players;
	current_offer[0] = 0;
	current_offer[1] = 10;
	int money_offered = 0;
	while(N_offerers > 1 && Fl::wait())
	{
		if(money_offered >= 0)
		{
			message.str("");
			money_offered = get_money_auction();
			//current_offer[0] = money_offered;
			//current_offer[1] = gamer_ID;
			if(N_offerers == 1 && current_offer[0]!= 0)
			{
				break;
			}
			synchronize();
			message << AUCTION <<"," << gamer_ID << "," << money_offered;
			//cout << message.str() << endl;

			list_of_messages.push(message.str());

		}
		else
		{
			synchronize();
		}
	}
	sleep(1);
	synchronize();
	offer_input->activate();
	offer_input->hide();
	not_interested_btn->activate();
	not_interested_btn->hide();
	offer_btn->activate();
	offer_btn->hide();
	gamer[current_offer[1]].money -= current_offer[0];
	info_text.str("");
	for(int i = 0; i< n_players; i++)
	{
		info_text<<"**********************************\nPlayer "<<i<<": "<<gamer[i].name<<endl;
		info_text<<"Money :"<<gamer[i].money<<"\nTerritories: "<<gamer[i].n_territories<<endl<<endl;
	}
	stringstream summary_message;
	if(gamer_ID == current_offer[1])
	{
		summary_message << "You";
	}
	else
	{
		summary_message << gamer[current_offer[1]].name;
	}
	summary_message << " won the auction\n";
	write_info_message(summary_message.str(), true);
	update_info_text();
	return current_offer[1];
	
}


void risikopoly_client::next_turn()
{
	synchronize();
	stringstream message;
	int n=1;
	message << NEXT_TURN << "," << gamer_ID;

	list_of_messages.push(message.str());
	send_all_messages();
}

void risikopoly_client::dice_attack()
{
	for(int i=0; i<3; i++)
	{
		atk_value[i] = def_value[i] = 0;
		yellow_die[i] = 0;
	}
	//Extract attack values 
	for(int i=0; i<armies_atk; i++)
	{
		atk_value[i] = rand() % 6 + 1;
	}

	//Extract defensive values 
	for(int i=0; i<armies_def; i++)
	{
		def_value[i] = rand() % 6 + 1;
		if(i < terr[attacked].n_defensive_armies)
		{
			yellow_die[i] = 1;
		}
	}
	
	//Sort dice values
	short int temp;
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
		{
			if(atk_value[j] < atk_value[i])
			{
				temp = atk_value[i];
				atk_value[i] = atk_value[j];
				atk_value[j] = temp;
			}
			if(def_value[j] < def_value[i])
			{
				temp = def_value[i];
				def_value[i] = def_value[j];
				def_value[j] = temp;
				temp = yellow_die[i];
				yellow_die[i] = yellow_die[j];
				yellow_die[j] = temp;
			}
		}
	}
	//send string message
	synchronize();
	stringstream message;
	int n = 1;
	message << DICE_FIGHT << "," << gamer_ID;
	message << "," << atk_value[0] << "," << atk_value[1] << "," << atk_value[2];
	message << "," << def_value[0] << "," << def_value[1] << "," << def_value[2];
	message << "," << yellow_die[0] << "," << yellow_die[1] << "," << yellow_die[2];

	list_of_messages.push(message.str());
	send_all_messages();
}

void risikopoly_client::fight()
{
	fighting = true;
	armies_atk = armies_def = 100;
	attacker = attacked = 100;
	bool continue_attack = true;
	for(int i = 0; i < N_TERRITORIES; i++)
	{
		decode_territory_name(i, gamer[terr[i].owner].color);
	}
	write_info_message("Choose the attacking territory", true);

	yes_btn->label("OK");
	yes_btn->show();
	yes_btn->clear();

	no_btn->label("SKIP");
	no_btn->show();
	no_btn->clear();

	while(1)
	{	
		check_buttons();
		synchronize();
		select_fighting_territories();
		Fl::wait(0.1);
		if(yes_btn->changed())
		{
			yes_btn->clear();
			if(attacker < N_TERRITORIES && attacked < N_TERRITORIES)
			{
				yes_btn->hide();
				army_display->hide();
				break;
			}
		}
		if(no_btn->changed())
		{
			no_btn->clear();
			continue_attack = false;
			army_display->hide();
			break;
		}
	}	
	yes_btn->label("YES");
	yes_btn->hide();

	no_btn->label("NO");
	no_btn->hide();

	while(continue_attack)
	{		
		check_buttons();
		stringstream message;
		synchronize();
		int n=1;				
		message.str("");
		message << SELECT_ATTACKER << "," << gamer_ID << "," <<  attacker << "," << attacked; 
		list_of_messages.push(message.str());
		send_all_messages();

		armies_def = 100;
		select_armies(1);
		while(armies_def > 3)
		{
			check_buttons();
			Fl::wait(0.1);
			synchronize();
		}
		dice_attack();
		if(terr[attacked].n_offensive_armies <=0 && terr[attacked].n_defensive_armies <=0) 
		{
			conquer_territory();
			break;
		}
		else if(terr[attacker].n_offensive_armies < 2)
		{
			break;
		}
		int choice = fl_choice("Do you want to continue?", "Yes", "No", 0);
		
		if(choice == 1)
		{
			continue_attack = false;
		}
	}
	fighting = false;
}


void risikopoly_client::prepare_game()
{
	write_info_message("Welcome to RISIKOPOLY, the best board game ever!\n", false);
	for(int i=0; i<N_TERRITORIES; i++)	//ASSIGNING EXTRA TERRITORIES THROUGH AUCTION
	{
		if(terr[i].owner == 100)
		{
			stringstream summary_message;
			summary_message<<"Assigning territory "<< terr[i].name <<endl;
			write_info_message(summary_message.str(), true);
			decode_territory_name(i, WHITE);
			terr[i].owner = auction();
			decode_territory_name(i, gamer[terr[i].owner].color);
		}
	}

	for(short int i=0; i<n_players; i++)
	{	
		gamer[i].position = 0;
	}
	player_turn = 0;
	gamer[player_turn].money += find_init_money(player_turn) * 15000;
}

void risikopoly_client::play_game()
{	
	draw_starting_screen();	
	process_stations();
	if(new_game)
	{
		prepare_game();
	}
	else while(!game_loaded)
	{
		Fl::wait(0.1);
		synchronize();
	}
	update_info_text();	
	for(short int i=0; i<n_players; i++)
	{	
		decode_pawn_name(i);
	}
	if(player_turn == gamer_ID)
	{
		write_info_message("YOUR TURN", true);
	}
	else
	{
		stringstream summary_message;
		summary_message << gamer[player_turn % n_players].name << "'s turn\n";
		write_info_message(summary_message.str(), false);
	}
	while(1)	//REAL GAME BEGINS NOW!!!
	{
		
		if(player_turn == gamer_ID)
		{	
			write_info_message("Phase 1: buy tanks", false);
			buy_tanks();		
			write_info_message("Phase 2: travel the cities", false);
			movement();
			for(int i=0; i<n_players; i++)
			{
				if(first_start_passed[i] == false)
				{
					break;
				}
				if(i == (n_players-1) && gamer[gamer_ID].state == 0)
				{					
					write_info_message("Phase 3: conquer other countries ", false);
					while(1)
					{
						fight();
						int result = fl_choice("Do you want to attack with another territory?", "Yes", "No", 0);
						if(result == 1)
						{
							break; 
						}
					}
					write_info_message("Phase 4: move your armies between territories ", false);
					move_tanks();
					if(attack_successfull)
					{
						stringstream message;
						message << WIN_TERR_CARD << "," << gamer_ID; 
						list_of_messages.push(message.str()); 
						send_all_messages();
					}
				}
			}

			//******only for debug	
			//fight();			
			//move_tanks();

			next_turn();
		}
		else 
		{
			check_buttons();
			Fl::wait(0.1);
			synchronize();
			if(under_attack == true)
			{
				select_armies();
			}
		}
		if(player_turn >= n_players)	player_turn = 0;
	}
}

void risikopoly_client::count_player_terr()
{
	int result=0;
	for(int N=0; N<n_players; N++)
	{
		result=0;
		for(int i=0; i<N_TERRITORIES; i++)
		{
			if(terr[i].owner == N)
			{
				result++;
			}
		}
		gamer[N].n_territories=result;
	}
}

bool risikopoly_client::contr_series_full(int contr_number)
{
	int contr_series_tot = 0;
	for(int i=0; i< N_CONTRACTS; i++)
	{
		if(contr[i].kind == contr[contr_number].kind && contr[i].mortgage == false && contr[i].owner == contr[contr_number].owner)
		{
			contr_series_tot ++;
		}
	}
	if(contr_series_tot == contract_series_full[contr[contr_number].kind])
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool risikopoly_client::build_house_ok(int contr_number)
{
	if(contr[contr_number].n_houses>=5 || contr_series_full(contr_number)==false || gamer[gamer_ID].money< contr[contr_number].price_house)
	{
		return false;
	}

	if(contr_number > 21)
	{
		return false;
	}
	for(int i=0; i< N_CONTRACTS; i++)
	{
		if(contr[contr_number].kind == contr[i].kind && contr[contr_number].n_houses > contr[i].n_houses)
		{
			return false;
		}
	}
	return true;
}

bool risikopoly_client::mortgage_ok(int contr_number)
{
	if(contr[contr_number].mortgage == true)
	{
		return false;
	}
	for(int i=0; i< N_CONTRACTS; i++)
	{
		if(contr[contr_number].kind == contr[i].kind && contr[i].n_houses > 0 && i <= 21)
		{
			return false;
		}
	}
	return true;
}

void risikopoly_client::house_construction(int contr_number)
{
	short int result;
	stringstream message, info_message, buy_message, sell_message, mortgage_message, demortgage_message;
	info_message.str("");
	message.str("");
	info_message << contr[contr_number].name << "\n";
	if(contr[contr_number].mortgage)
	{
		info_message << "MORTGAGED \n";
	}
	else
	{
		int n_houses = contr[contr_number].n_houses;
		if(contr_number <= 21)
		{
			info_message << "Number of houses: " << n_houses << "\n";
		}
		int charge = contr[contr_number].value_house[n_houses];
		if(n_houses == 0 && contr_series_full(contr_number) == true && contr_number <= 21)
		{
			charge = contr[contr_number].value_house[0] * 2;
		}
		info_message << "Charge: " << charge << "\n";
	}
	info_message << "OWNER: " << gamer[contr[contr_number].owner].name;
	
	buy_message.str("");
	buy_message << "Buy House (- " << (contr[contr_number].price_house) << " ctk)";
	sell_message.str("");
	if(contr[contr_number].n_houses < 5)
	{
		sell_message << "Sell House (+ " << (contr[contr_number].price_house/2) << " ctk)";
	}
	else
	{
		sell_message << "Sell House (+ " << (contr[contr_number].price_house/2*5) << " ctk)";
	}
	mortgage_message.str("");
	mortgage_message << "Mortgage (+ " << (contr[contr_number].price/2) << " ctk)";
	demortgage_message.str("");
	demortgage_message << "De-mortgage (- " << (contr[contr_number].price/2 + contr[contr_number].price/10) << " ctk)";


	if(player_turn != gamer_ID || contr[contr_number].owner != gamer_ID)
	{
		fl_message("%s", info_message.str().c_str());
	}	
	else
	{
		if(contr[contr_number].mortgage == true)
		{
			if(gamer[gamer_ID].money >= (contr[contr_number].price*2 + contr[contr_number].price/10))
			{
				result = fl_choice("%s", "Exit", demortgage_message.str().c_str(), 0, info_message.str().c_str());
				if (result == 1)
				{
					//send demortgage message : 3
					message << BUY_HOUSE << "," << gamer_ID << "," << contr_number << "," << 3;
				}
				
			}
			else
			{
				fl_message("%s", info_message.str().c_str());
			}	
		}
		else if(contr_series_full(contr_number)==false)
		{
			if(mortgage_ok(contr_number))
			{
				result = fl_choice("%s", "Exit", mortgage_message.str().c_str(), 0, info_message.str().c_str());
				if (result == 1)
				{
					//send mortgage message : 2
					message << BUY_HOUSE << "," << gamer_ID << "," << contr_number << "," << 2;
				}
			}
			else
			{
				fl_message("%s", info_message.str().c_str());
			}
		}
		else
		{
			if(build_house_ok(contr_number))
			{
				if(contr[contr_number].n_houses > 0 && contr_number <= 21)
				{
					result = fl_choice("%s", "Exit", buy_message.str().c_str(), sell_message.str().c_str(), info_message.str().c_str());
					if(result == 1)
					{
						//send buy message : 0
						message << BUY_HOUSE << "," << gamer_ID << "," << contr_number << "," << 0;
					}
					if(result == 2)
					{
						//send sell message : 1
						message << BUY_HOUSE << "," << gamer_ID << "," << contr_number << "," << 1;
					}
				}
				else if(mortgage_ok(contr_number))
				{
					result = fl_choice("%s", "Exit", buy_message.str().c_str(), mortgage_message.str().c_str(), info_message.str().c_str());
					if(result == 1)
					{
						//send buy message : 0
						message << BUY_HOUSE << "," << gamer_ID << "," << contr_number << "," << 0;
					}
					if(result == 2)
					{
						//send mortgage message : 2
						message << BUY_HOUSE << "," << gamer_ID << "," << contr_number << "," << 2;
					}
				}
				else
				{
					result = fl_choice("%s", "Exit", buy_message.str().c_str(), 0, info_message.str().c_str());
					if(result == 1)
					{
						//send buy message : 0
						message << BUY_HOUSE << "," << gamer_ID << "," << contr_number << "," << 0;
					}
				}
				
			}
			else
			{
				if(contr[contr_number].n_houses > 0 && contr_number <= 21)
				{					
					result = fl_choice("%s", "Exit", sell_message.str().c_str(), 0, info_message.str().c_str());
					if(result == 1)
					{
						//send sell message : 1
						message << BUY_HOUSE << "," << gamer_ID << "," << contr_number << "," << 1;
					}
				}
				else if(mortgage_ok(contr_number))
				{
					result = fl_choice("%s", "Exit", mortgage_message.str().c_str(), 0, info_message.str().c_str());
					if(result == 1)
					{
						//send mortgage message : 2
						message << BUY_HOUSE << "," << gamer_ID << "," << contr_number << "," << 2;
					}
				}
				else
				{
					fl_message("%s", info_message.str().c_str());
				}	
			}
		}
	}
	if(message.str() != "")
	{
		int n=1;
		list_of_messages.push(message.str());
		send_all_messages();
	}
}

void risikopoly_client::proceed_destination(short int Player)
{
	stringstream message, info_string;

	for(int i=0; i< N_CONTRACTS; i++)
	{
		
		int owner = contr[i].owner;
		if(contract_position[i]==gamer[Player].position)
		{
			if(Player==gamer_ID)
			{
				if(contr[i].owner >=n_players)
				{
					message << BUY_OR_AUCTION << "," << gamer_ID << "," << i;
					list_of_messages.push(message.str());
				}
				else if(contr[i].owner != gamer_ID)
				{	
					info_string << "You stepped into " << contr[position_contract[gamer[gamer_ID].position]].name << ". Owner: " << gamer[owner].name;
					info_string << ". Price " << contr[i].value_house[contr[i].n_houses];	
					write_info_message(info_string.str(), true);
					payment_needed = true;
					message << PAY << "," << gamer_ID;
					list_of_messages.push(message.str());
				}
				else
				{
					info_string << "You stepped on " << "your territory " << contr[i].name;
					info_string << ". Nothing happens";
					write_info_message(info_string.str(), true);
				}
			}
			else if(owner == gamer_ID)
			{
				info_string << gamer[Player].name << " stepped on your territory " << contr[i].name;
				info_string << ". He has to pay " << contr[i].value_house[contr[i].n_houses] << " ctk";
				write_info_message(info_string.str(), true);
			}
			else if(owner != Player && owner < n_players)
			{
				info_string << gamer[Player].name << " stepped on " << gamer[owner].name << "'s territory " << contr[i].name;
				info_string << ". He has to pay " << contr[i].value_house[contr[i].n_houses] << " ctk";
				write_info_message(info_string.str(), true);
			}
			else if(owner == Player)
			{
				info_string << gamer[Player].name << " stepped on " << "his territory " << contr[i].name;
				info_string << ". Nothing happens";
				write_info_message(info_string.str(), true);
			}
		}
	}
	//chance cards
	if(gamer[Player].position == 7 || gamer[Player].position == 22 || gamer[Player].position == 36)
	{		
		int chance_picked = chance_deck.front();
		chance_deck.pop();
		if(Player == gamer_ID)
		{
			info_string << "You stepped on a Chance place\n";
		}
		else
		{			
			info_string << gamer[Player].name << " stepped on a Chance place\n";
		}
		info_string << chance_name[chance_picked] << endl;	
		write_info_message(info_string.str(), true);
		decode_chance(Player, chance_picked);
	}

	//chest cards
	else if(gamer[Player].position == 2 || gamer[Player].position == 17 || gamer[Player].position == 33)
	{		
		int chest_picked = chest_deck.front();
		chest_deck.pop();
		if(Player == gamer_ID)
		{
			info_string << "You stepped on a Common Chest place\n";
		}
		else
		{			
			info_string << gamer[Player].name << " stepped on a Common Chest place\n";
		}
		info_string << chest_name[chest_picked] << endl;	
		write_info_message(info_string.str(), true);
		decode_chest(Player, chest_picked);
	}
	
	//prison
	else if(gamer[Player].position == 30)
	{
		gamer[Player].position = 10;
		decode_pawn_name(Player);
		gamer[Player].state = 2;
		turnsInPrison[Player] = 0;
		update_info_text();
		if(Player == gamer_ID)
		{
			info_string << "You went to prison\n";
		}
		else
		{			
			info_string << gamer[Player].name << " went to prison\n";
		}
		write_info_message(info_string.str(), true);
	}

	//free parking
	else if(gamer[Player].position == 10 || gamer[Player].position == 20)
	{
		if(Player == gamer_ID)
		{
			info_string << "You stepped on a free parking, nothing happens\n";
		}
		else
		{			
			info_string << gamer[Player].name << " stepped on a free parking, nothing happens\n";
		}
		write_info_message(info_string.str(), true);
	}

	//income tax
	else if(gamer[Player].position == 4)
	{
		if(Player == gamer_ID)
		{
			//payment_needed = true;
			info_string << "You stepped on 'Income tax', you have to pay 20000 ctk\n";
			write_info_message(info_string.str(), true);
			payment(false, 20000);
		}
		else
		{
			info_string << gamer[Player].name << " stepped on 'Income tax', " << gamer[Player].name << " has to pay 20000 ctk\n";
			write_info_message(info_string.str(), true);
		}
	}

	//super tax
	else if(gamer[Player].position == 38)
	{
		if(Player == gamer_ID)
		{
			//payment_needed = true;
			info_string << "You stepped on 'Super tax', you have to pay 15000 ctk\n";
			write_info_message(info_string.str(), true);
			payment(false, 15000);
		}
		else
		{
			info_string << gamer[Player].name << " stepped on 'Super tax', " << gamer[Player].name << " has to pay 15000 ctk\n";
			write_info_message(info_string.str(), true);
		}
	}
}


bool risikopoly_client::check_offer_validity()
{
	if(gamer[exchange_offerer].money < offered_items[0]) 
	{
		return false;
	}
	if (gamer[exchange_receiver].money < desidered_items[0])
	{
		return false;
	}
	for(int i=0; i<N_CONTRACTS; i++)
	{
		if(desidered_items[4+i] == 1)
		{
			if(contr[i].owner != exchange_receiver || contr[i].n_houses > 0)
			{
				return false;
			}
		}

		if(offered_items[4+i] == 1)
		{
			if(contr[i].owner != exchange_offerer || contr[i].n_houses > 0)
			{
				return false;
			}
		}

	}

	return true;
}

void risikopoly_client::sell_tanks(int n_terr)
{
	stringstream message, info_message, alltanks_message;
	info_message << terr[n_terr].name << "\n";
	int owner = terr[n_terr].owner;
	if(owner == gamer_ID)
	{
		info_message << "Owner : you\n";
	}
	else
	{
		info_message << "Owner : " << (owner < n_players ? gamer[owner].name : "No one") << endl;
	}
	info_message << "Number of tanks: " << terr[n_terr].n_offensive_armies;
	if(owner == gamer_ID && terr[n_terr].n_offensive_armies > 1 && (player_turn == gamer_ID || payment_needed == true))
	{
		alltanks_message << "Sell all tanks (+";
		int extra_money = 7500 * (terr[n_terr].n_offensive_armies - 1);
		alltanks_message << extra_money << " ctk)";
		int choice = fl_choice("%s", "Exit", "Sell 1 tank (+7500 ctk)", alltanks_message.str().c_str(), info_message.str().c_str());
		cout << "CHOICE: " << choice << endl;
		if(choice == 1)
		{
			message << SELLTANK << "," << gamer_ID << "," << n_terr << "," <<  1;		
			list_of_messages.push(message.str());
		}
		else if(choice == 2)
		{
			message << SELLTANK << "," << gamer_ID << "," <<  n_terr <<  "," << (terr[n_terr].n_offensive_armies - 1);
			list_of_messages.push(message.str());
		}
	}
	else
	{
		fl_message("%s", info_message.str().c_str());
	}

}

void risikopoly_client::move_tanks()
{
	fighting = true;
	armies_atk = armies_def = 100;
	attacker = attacked = 100;
	bool continue_move = true;
	for(int i = 0; i < N_TERRITORIES; i++)
	{
		decode_territory_name(i, gamer[terr[i].owner].color);
	}
	write_info_message("Now is your chance to move armies between your territories\nSelect territory to donate armies", true);

	yes_btn->label("OK");
	yes_btn->show();
	yes_btn->clear();

	no_btn->label("SKIP");
	no_btn->show();
	no_btn->clear();

	while(1)
	{	
		check_buttons();
		synchronize();
		select_fighting_territories(false);
		Fl::wait(0.1);
		if(yes_btn->changed())
		{
			yes_btn->clear();
			if(attacker < N_TERRITORIES && attacked < N_TERRITORIES)
			{
				yes_btn->hide();
				army_display->hide();
				break;
			}
		}
		if(no_btn->changed())
		{
			no_btn->clear();
			continue_move = false;
			army_display->hide();
			break;
		}
	}	
	yes_btn->label("YES");
	yes_btn->hide();

	no_btn->label("NO");
	no_btn->hide();
	if(continue_move)
	{
		select_armies(2);
	}
	attacker = attacked = 100;
	fighting = false;
}

int risikopoly_client::find_init_money(int PLAYER)
{
	count_player_terr();
	int result = gamer[PLAYER].n_territories/3;
	stringstream summary_message;
	const char *subject = (PLAYER == gamer_ID ? "You" : gamer[PLAYER].name);
	const char *verb = (PLAYER == gamer_ID ? " have" : " has"); 
	summary_message << subject << verb << " " << gamer[PLAYER].n_territories << " territories, +" << 15000*result << " ctk\n";

	//check for AMERICA
	for(int i=0; i<9; i++)
	{
		if(terr[i].owner != PLAYER)
		{
			break;
		}
		else if(i == 8)
		{
			if(contr[8].owner == PLAYER && contr[9].owner == PLAYER && contr[10].owner == PLAYER) //check for blue contracts
			{
				summary_message << subject << verb << " the entire NORTH AMERICA and the brown contracts, +105000 ctk.\n";
				result += 7; 
			}
			else
			{
				summary_message << subject << verb << " the entire NORTH AMERICA, +75000 ctk.\n";
				result += 5;
			}
	
		}
	}
	//check for SUDAMERICA
	for(int i=0; i<4; i++)
	{
		if(terr[9+i].owner != PLAYER)
		{
			break;
		}
		else if(i == 3)
		{
			if(contr[17].owner == PLAYER && contr[18].owner == PLAYER && contr[19].owner == PLAYER) //check for green contracts
			{
				summary_message << subject << verb << " the entire SUD AMERICA and the green contracts, +60000 ctk.\n";
				result += 4; 
			}
			else
			{
				summary_message << subject << verb << " the entire SUD AMERICA, +30000 ctk.\n";
				result += 2;
			}
	
		}
	}
	//check for SUDAMERICA
	for(int i=0; i<4; i++)
	{
		if(terr[13+i].owner != PLAYER)
		{
			break;
		}
		else if(i == 3)
		{
			if(contr[20].owner == PLAYER && contr[21].owner == PLAYER ) //check for purple contracts
			{
				summary_message << subject << verb << " the entire OCEANIA and the purple contracts, +60000 ctk.\n";
				result += 4; 
			}
			else
			{
				summary_message << subject << verb << " the entire OCEANIA, +30000 ctk.\n";
				result += 2;
			}
	
		}
	}
	//check for EUROPE
	for(int i=0; i<7; i++)
	{
		if(terr[17+i].owner != PLAYER)
		{
			break;
		}
		else if(i == 6)
		{
			if(contr[5].owner == PLAYER && contr[6].owner == PLAYER && contr[7].owner == PLAYER) //check for orange contracts
			{
				summary_message << subject << verb << " the entire EUROPE and the orange contracts, +105000 ctk.\n";
				result += 7; 
			}
			else
			{
				summary_message << subject << verb << " the entire EUROPE, +75000 ctk.\n";
				result += 5;
			}
	
		}
	}
	//check for AFRICA
	for(int i=0; i<6; i++)
	{
		if(terr[24+i].owner != PLAYER)
		{
			break;
		}
		else if(i == 5)
		{
			if(contr[11].owner == PLAYER && contr[12].owner == PLAYER && contr[13].owner == PLAYER) //check for orange contracts
			{
				summary_message << subject << verb << " the entire AFRICA and the red contracts, +75000 ctk.\n";
				result += 5; 
			}
			else
			{
				summary_message << subject << verb << " the entire AFRICA, +45000 ctk.\n";
				result += 3;
			}
	
		}
	}
	//check for ANTARTICA
	for(int i=0; i<2; i++)
	{
		if(terr[30+i].owner != PLAYER)
		{
			break;
		}
		else if(i == 1)
		{
			if(contr[0].owner == PLAYER && contr[1].owner == PLAYER) //check for orange contracts
			{
				summary_message << subject << verb << " the entire ANTARTICA and the pink contracts, +45000 ctk.\n";
				result += 3; 
			}
			else
			{
				summary_message << subject << verb << " the entire ANTARTICA, +15000 ctk.\n";
				result += 1;
			}
	
		}
	}
	//check for ASIA
	for(int i=0; i<12; i++)
	{
		if(terr[32+i].owner != PLAYER)
		{
			break;
		}
		else if(i == 11)
		{
			if(contr[2].owner == PLAYER && contr[3].owner == PLAYER && contr[4].owner == PLAYER) //check for orange contracts
			{
				summary_message << subject << verb << " the entire ASIA and the blue contracts, +135000 ctk.\n";
				result += 9; 
			}
			else
			{
				summary_message << subject << verb << " the entire ASIA, +105000 ctk.\n";
				result += 7;
			}
	
		}
	}
	write_info_message(summary_message.str(), true);
	return result;
}

void risikopoly_client::decode_chance(int Player, int chance_picked)
{
	switch (chance_picked)
	{
		case 0 : case 7://pay 15000
		{
			if(Player == gamer_ID)
			{
				//payment_needed = true;
				payment(false, 15000);
			}
			chance_deck.push(chance_picked);
			break;
		}
		case 1://pay 2000
		{
			if(Player == gamer_ID)
			{
				//payment_needed = true;
				payment(false, 2000);
			}
			chance_deck.push(chance_picked);
			break;
		}
		case 2://go to Largo Colombo
		{
			if(gamer[Player].position > 24)
			{
				gamer[Player].money += 20000;
				first_start_passed[Player] = true;
				if (Player == gamer_ID)
				{
					fl_message("You passed through the start, you get 20000 ctk!");
				}
			}
			gamer[Player].position = 24;
			decode_pawn_name(Player);
			proceed_destination(Player);
			chance_deck.push(chance_picked);
			break;
		}
		case 3://go to Parco della Vittoria
		{
			gamer[Player].position = 39;
			decode_pawn_name(Player);
			proceed_destination(Player);
			chance_deck.push(chance_picked);
			break;
		}
		case 4://pay 4000 houses, 10000 hotels 
		{
			int money_to_pay = 0;
			for(int i=0; i<N_CONTRACTS; i++)
			{
				if(contr[i].owner == Player)
				{
					if(contr[i].n_houses == 5)
					{
						money_to_pay += 10000;
					}
					else
					{
						money_to_pay += 4000 * contr[i].n_houses;
					}
				}
			}
			if(Player == gamer_ID)
			{
				stringstream summary_message;
				summary_message << "You have to pay " << money_to_pay << " ctk. Sigh!\n";
				write_info_message(summary_message.str(), true);
				//payment_needed = true;
				payment(false, money_to_pay);
			}
			else
			{
				stringstream summary_message;
				summary_message << gamer[Player].name << " has to pay " << money_to_pay << " ctk. Ahah!\n";
				write_info_message(summary_message.str(), true);
			}
			chance_deck.push(chance_picked);
			break;
		}

		case 5://go to Via Accademia
		{
			if(gamer[Player].position > 11)
			{
				gamer[Player].money += 20000;
				first_start_passed[Player] = true;
				if (Player == gamer_ID)
				{
					fl_message("You passed through the start, you get 20000 ctk!");
				}
			}
			gamer[Player].position = 11;
			decode_pawn_name(Player);
			proceed_destination(Player);
			chance_deck.push(chance_picked);
			break;
		}
		case 6://pay 2500 houses, 10000 hotels 
		{
			int money_to_pay = 0;
			for(int i=0; i<N_CONTRACTS; i++)
			{
				if(contr[i].owner == Player)
				{
					if(contr[i].n_houses == 5)
					{
						money_to_pay += 10000;
					}
					else
					{
						money_to_pay += 2500 * contr[i].n_houses;
					}
				}
			}
			if(Player == gamer_ID)
			{
				stringstream summary_message;
				summary_message << "You have to pay " << money_to_pay << " ctk. Sigh!\n";
				write_info_message(summary_message.str(), true);
				//payment_needed = true;
				payment(false, money_to_pay);
			}
			else
			{
				stringstream summary_message;
				summary_message << gamer[Player].name << " has to pay " << money_to_pay << " ctk. Ahah!\n";
				write_info_message(summary_message.str(), true);
			}
			chance_deck.push(chance_picked);
			break;
		}
		case 8: case 9://get 15000 ctk 
		{
			gamer[Player].money += 15000;
			chance_deck.push(chance_picked);
			break;
		}
		case 10://go to Stazione Nord
		{
			if(gamer[Player].position > 25)
			{
				gamer[Player].money += 20000;
				first_start_passed[Player] = true;
				if (Player == gamer_ID)
				{
					fl_message("You passed through the start, you get 20000 ctk!");
				}
			}
			gamer[Player].position = 25;
			decode_pawn_name(Player);
			proceed_destination(Player);
			chance_deck.push(chance_picked);
			break;
		}
		case 11://get 5000 ctk 
		{
			gamer[Player].money += 5000;
			chance_deck.push(chance_picked);
			break;
		}
		case 12://go 3 steps back
		{
			gamer[Player].position -= 3;
			decode_pawn_name(Player);
			proceed_destination(Player);
			chance_deck.push(chance_picked);
			break;
		}
		case 13://go to Via
		{
			gamer[Player].money += 20000;
			first_start_passed[Player] = true;
			if (Player == gamer_ID)
			{
				fl_message("You passed through the start, you get 20000 ctk!");
			}
			gamer[Player].position = 0;
			decode_pawn_name(Player);
			chance_deck.push(chance_picked);
			break;
		}
		case 14: //go to prison
		{
			gamer[Player].position = 10;
			gamer[Player].state = 2;
			turnsInPrison[Player] = 0;
			decode_pawn_name(Player);
			chance_deck.push(chance_picked);
			break;
		}
		case 15: //exit from prison
		{
			chance_card[15].owner == Player;
			break;
		}
		case 16: //prize for stations
		{
			int n_stations = find_station_player(Player);
			stringstream summary_message;
			if(Player == gamer_ID)
			{
				summary_message << "You own " << n_stations << " stations\n";
			}
			else
			{
				summary_message << gamer[Player].name << " owns " << n_stations << " stations\n";
			}
			if(n_stations == 0) //you lose 10 tanks
			{
				int max_tanks = max_tank_remove(Player);
				bool tank_over_max = max_tanks <= 10;
				int tanks_to_remove = 10;
				if(tank_over_max)
				{
					tanks_to_remove = max_tanks;
				}
				if(Player == gamer_ID)
				{
					summary_message << "You lose " << (tank_over_max ? "all yours extra" : "10") << " tanks\nSelect territories to remove tanks from!";
					write_info_message(summary_message.str(), true);
					remove_tanks(tanks_to_remove, true);
				}
				else
				{
					summary_message << gamer[Player].name << " loses " << (tank_over_max ? "all his extra" : "10") << " tanks\n";
					write_info_message(summary_message.str(), true);
				}
			}
			else if(n_stations == 1) //no effect
			{
				summary_message << "Nothing happens\n";
				write_info_message(summary_message.str(), true);
			}
			else if(n_stations == 2) //win 3 tanks
			{
				if(Player == gamer_ID)
				{
					summary_message << "You win 3 tanks\nSelect territories to put tanks in!";
					write_info_message(summary_message.str(), true);
					dispose_tanks(3);
				}
				else
				{
					summary_message << gamer[Player].name << " wins 3 tanks\n";
					write_info_message(summary_message.str(), true);
				}
			}
			else if(n_stations ==3) //remove 5 tanks
			{
				int max_tanks = 0;
				for(int i=0; i<n_players; i++)
				{
					max_tanks +=max_tank_remove(i);
				}
				bool tank_over_max = max_tanks <= 5;
				int tanks_to_remove = 5;
				if(tank_over_max)
				{
					tanks_to_remove = max_tanks;
				}
				if(Player == gamer_ID)
				{
					summary_message << "You have to remove " << tanks_to_remove << " tanks (you can choose from any territory).\nSelect territories toremove tanks from!";
					write_info_message(summary_message.str(), true);
					remove_tanks(tanks_to_remove, false);
				}
				else
				{
					summary_message << gamer[Player].name << " removes " << tanks_to_remove << " tanks \n";
					write_info_message(summary_message.str(), true);
				}
			}
			else if(n_stations ==4) //win 5 tanks, remove 5 tanks
			{
				int max_tanks = 0;
				for(int i=0; i<n_players; i++)
				{
					max_tanks +=max_tank_remove(i);
				}
				bool tank_over_max = max_tanks <= 5;
				int tanks_to_remove = 5;
				if(tank_over_max)
				{
					tanks_to_remove = max_tanks;
				}
				if(Player == gamer_ID)
				{
					summary_message << "You win 5 tanks and you remove " << tanks_to_remove << " tanks (you can choose from any territory).\nSelect territories to put tanks in!";
					write_info_message(summary_message.str(), true);
					dispose_tanks(5);
					send_all_messages();
					write_info_message("Select territories to remove tanks from!", true);
					remove_tanks(tanks_to_remove, false);
				}
				else
				{
					summary_message << gamer[Player].name << " wins 5 tanks and has to remove " << tanks_to_remove << " tanks \n";
					write_info_message(summary_message.str(), true);
				}
			}
			else
			{
				summary_message << "Error!!!\n";
				write_info_message(summary_message.str(), true);
			}
			chance_deck.push(chance_picked);
			break;
		}
		case 17: //swap place
		{

		}
		default:
		{
			break;
		}
	}
	update_info_text();
}

void risikopoly_client::decode_chest(int Player, int chest_picked)
{
	switch (chest_picked)
	{
		case 0 ://if Jap -> get 150000
		{
			if(terr[43].owner == Player)
			{
				gamer[Player].money += 150000;
			}
			chest_deck.push(chest_picked);
			break;
		}
		case 1: case 9://pay 5000
		{
			if(Player == gamer_ID)
			{
				//payment_needed = true;
				payment(false, 5000);
			}
			chest_deck.push(chest_picked);
			break;
		}

		case 2://pay 10000
		{
			if(Player == gamer_ID)
			{
				//payment_needed = true;
				payment(false, 10000);
			}
			chest_deck.push(chest_picked);
			break;
		}

		case 3://get 2500
		{
			gamer[Player].money += 2500;
			chest_deck.push(chest_picked);
			break;
		}

		case 4://get 10000 from each player
		{
			if(Player != gamer_ID)
			{
				//payment_needed = true;
				payment(false, 10000, Player);
			}
			chest_deck.push(chest_picked);
			break;
		}

		case 5: //go to prison
		{
			gamer[Player].position = 10;
			decode_pawn_name(Player);
			gamer[Player].state = 2;
			turnsInPrison[Player] = 0;
			chest_deck.push(chest_picked);
			break;
		}

		case 6: case 15://get 5000
		{
			gamer[Player].money += 5000;
			chest_deck.push(chest_picked);
			break;
		}

		case 7: case 10://get 10000
		{
			gamer[Player].money += 10000;
			chest_deck.push(chest_picked);
			break;
		}

		case 8://go to Via
		{
			gamer[Player].money += 20000;
			first_start_passed[Player] = true;
			if (Player == gamer_ID)
			{
				fl_message("You passed through the start, you get 20000 ctk!");
			}
			gamer[Player].position = 0;
			decode_pawn_name(Player);
			chest_deck.push(chest_picked);
			break;
		}

		case 11: //exit from prison
		{
			chest_card[11].owner = Player;
		}

		case 12://go to Vicolo Corto
		{
			gamer[Player].position = 1;
			decode_pawn_name(Player);
			proceed_destination(Player);
			chest_deck.push(chest_picked);
			break;
		}

		case 13: //ticket or chance
		{
			stringstream message;
			if (Player == gamer_ID)
			{
				int choice = fl_choice("Choose wisely", "Ticket", "Chance", 0);
				if(choice == 1)
				{
					message << DRAW_CHANCE << "," << gamer_ID;
				}
				else 
				{
					message << PAY_TICKET << "," << gamer_ID;
				}
				list_of_messages.push(message.str()); 
				send_all_messages();
			}
			chest_deck.push(chest_picked);
			break;
		}

		case 14://get 20000
		{
			gamer[Player].money += 20000;
			chest_deck.push(chest_picked);
			break;
		}

		case 16://if Player owns Japan he wins 3 tanks
		{
			bool isOwning = terr[43].owner == Player;
			stringstream summary_message;
			if(gamer_ID == Player)
			{
				if(isOwning)
				{
					summary_message << "You own Japan, you win 3 tanks\n";
					write_info_message(summary_message.str(), true);
					dispose_tanks(3);
				}
				else
				{
					summary_message << "You do not own Japan, shame on you!\n";
					write_info_message(summary_message.str(), true);
				}
			}
			else
			{
				if(isOwning)
				{
					summary_message << gamer[Player].name << " owns Japan, he wins 3 tanks\n";
					write_info_message(summary_message.str(), true);
				}
				else
				{
					summary_message << gamer[Player].name << " doesn't own Japan, he lost his chance\n";
					write_info_message(summary_message.str(), true);
				}
			}
			chest_deck.push(chest_picked);
			break;
		}

		default:
		{
			break;
		}
	}
	update_info_text();
}

void risikopoly_client::process_stations()
{
	queue <short int> stations[6], societies[6];
	for(int i=0; i<n_players; i++)
	{
		// check stations
		for(int j=0; j<4; j++)
		{
			if(contr[22 + j].owner == i && contr[22 + j].mortgage == false)
			{
				stations[i].push(22 + j);
			}
		}
		int size = stations[i].size() - 1;
		while(stations[i].size() > 0)
		{
			contr[stations[i].front()].n_houses = size;
			stations[i].pop();
		}
		// check società elettrica and società acqua potabile
		for(int j=0; j<2; j++)
		{
			if(contr[26 + j].owner == i && contr[26 + j].mortgage == false)
			{
				societies[i].push(26 + j);
			}
		}
		size = societies[i].size() - 1;
		while(societies[i].size() > 0)
		{
			contr[societies[i].front()].n_houses = size;
			societies[i].pop();
		}
	}
}

int risikopoly_client::find_station_player(int Player)
{
	queue <short int> stations;
	// check stations
	for(int j=0; j<4; j++)
	{
		if(contr[22 + j].owner == Player)
		{
			stations.push(22 + j);
		}
	}
	return stations.size();
}

void risikopoly_client::save()
{
	ofstream output("savings/Data.sav");
	output << gamer_ID << " " << n_players << " " << player_turn << "\n";
	//players info
	decode_terr_card();
	for(int i=0; i<n_players; i++)
	{
		output << gamer[i].name << "  ";
		output << gamer[i].position << " ";
		output << gamer[i].color << " ";
		output << gamer[i].n_territories << " ";
		output << gamer[i].money << " ";

		output << first_start_passed[i] << " ";
		output << turnsInPrison[i] << " ";
		output << n_cannons[i] << " ";
		output << n_pedestrians[i] << " ";
		output << n_horses[i] << " "; 
		output << n_jollies[i] << "\n";
	}
	//territories info
	for(int i=0; i<N_TERRITORIES; i++)
	{
		output << terr[i].owner << " ";
		output << terr[i].n_offensive_armies << "\n";
	}
	//contracts info
	for(int i=0; i<N_CONTRACTS; i++)
	{
		output << contr[i].owner << " ";
		output << contr[i].n_houses << " ";
		output << (contr[i].mortgage ? 1 : 0) << "\n";
	}
	//territory cards info
	for(int i=0; i<N_TERRITORY_CARDS; i++)
	{
		output << terr_card[i].owner << "\n";
	}
	//chance cards info
	for(int i=0; i<CHANCE_CARDS; i++)
	{
		output << chance_card[i].owner << "\n";
	}
	//chest cards info
	for(int i=0; i<N_TERRITORY_CARDS; i++)
	{
		output << chest_card[i].owner << "\n";
	}
	//territory cards deck
	output << terr_card_deck.size();
	for(unsigned int i=0; i<terr_card_deck.size(); i++)
	{
		int temp = terr_card_deck.front();
		terr_card_deck.pop();
		output << " " << temp;
		terr_card_deck.push(temp);
	}
	output << "\n";
	//chance cards deck
	output << chance_deck.size();
	for(unsigned int i=0; i<chance_deck.size(); i++)
	{
		int temp = chance_deck.front();
		chance_deck.pop();
		output << " " << temp;
		chance_deck.push(temp);
	}
	output << "\n";
	//chest cards deck
	output << chest_deck.size();
	for(unsigned int i=0; i<chest_deck.size(); i++)
	{
		int temp = chest_deck.front();
		chest_deck.pop();
		output << " " << temp;
		chest_deck.push(temp);
	}
	output << "\n";
	
	output.close();
}

void risikopoly_client::load()
{
	cout << "Loading...\n";
	ifstream input("savings/Data.sav");
	if(!input)
	{
		cout << "Cannot open saving file... Aborting\n";
		return;
	}
	input >> gamer_ID;
	input >> n_players;
	input >> player_turn;
	cout << "10%...";
	//players info
	for(int i=0; i<n_players; i++)
	{
		char c;
		stringstream name;
		name.str("");
		while(1)
		{
			input.get(c);
			if(c == ' ')
			{
				break;
			}
			else if(c != '\n')
			{
				name << c;
			}
		}
		gamer[i].name = new char[name.str().size()];
		strcpy(gamer[i].name, name.str().c_str());
		input >> gamer[i].position;
		input >> gamer[i].color;
		input >> gamer[i].n_territories;
		input >> gamer[i].money;
		input >> first_start_passed[i];
		input >> turnsInPrison[i];
		input >> n_cannons[i];
		input >> n_pedestrians[i];
		input >> n_horses[i]; 
		input >> n_jollies[i];
	}
	cout << "20%...";
	//territories info
	for(int i=0; i<N_TERRITORIES; i++)
	{
		input >> terr[i].owner;
		input >> terr[i].n_offensive_armies;
	}
	cout << "30%...";
	//contracts info
	for(int i=0; i<N_CONTRACTS; i++)
	{
		input >> contr[i].owner;
		input >> contr[i].n_houses;
		input >> contr[i].mortgage;
	}
	cout << "40%...";
	//territory cards info
	for(int i=0; i<N_TERRITORY_CARDS; i++)
	{
		input >> terr_card[i].owner;
	}
	//chance cards info
	for(int i=0; i<CHANCE_CARDS; i++)
	{
		input >> chance_card[i].owner;
	}
	cout << "50%...";
	
	//chest cards info
	for(int i=0; i<N_TERRITORY_CARDS; i++)
	{
		input >> chest_card[i].owner;
	}
	//territory cards deck
	int size, temp;
	input >> size;
	for(int i=0; i<size; i++)
	{
		input >> temp;
		terr_card_deck.push(temp);
	}
	cout << "60%...";

	//chance cards deck
	input >> size;
	for(int i=0; i<size; i++)
	{
		input >> temp;
		chance_deck.push(temp);
	}
	cout << "70%...";
	//chest cards deck
	input >> size;
	for(int i=0; i<size; i++)
	{
		input >> temp;
		chest_deck.push(temp);
	}
	cout << "80%...";
	cout << "90%...";
	
	input.close();
	cout << "100%...\n";

	write_info_message("Welcome back to RISIKOPOLY, the best board game ever!\n", false);	
	update_info_text();
	game_loaded = true;
}

int risikopoly_client::max_tank_remove(int Player)
{
	int result = 0;
	for(int i=0; i<N_TERRITORIES; i++)
	{
		if(terr[i].owner == Player)
		{
			result += (terr[i].n_offensive_armies - 1);
		}
	}
	return result;
}

void risikopoly_client::pay_from_cb(int amount)
{

}
