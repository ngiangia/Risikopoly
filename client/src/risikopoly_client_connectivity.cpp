#include"risikopoly_client.h"
#include "string.h"

int risikopoly_client::send_message(string message)
{
	int client_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(client_sockfd < 0)
	{
		perror("ERROR, cannot open socket");
		fl_message("ERROR, cannot open socket");
		close(client_sockfd);
		return 1;
	}
	server = gethostbyname(host_ip.c_str());
	if (server == NULL) 
	{
		perror("ERROR, no such host");
		fl_message("ERROR, no such host");
		close(client_sockfd);
		return -1;
	
	}
	memset(((char *) &serv_addr), (0), (sizeof(serv_addr)));
	serv_addr.sin_family = AF_INET;
	memmove(((char *)&serv_addr.sin_addr.s_addr), ((char *)server->h_addr), (server->h_length));
	serv_addr.sin_port = htons(5000);
	if (connect(client_sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
	{
		perror("ERROR connecting");
		fl_message("ERROR connecting");
		close(client_sockfd);
		return 1;
	}
	int n = write(client_sockfd, message.c_str(), message.length());
	if (n < 0) 
	{
		perror("ERROR writing to socket");
		close(client_sockfd);
		return -3;
	}
	else
	{
		//cout << "Connected to host\n";
		memset(sock_buffer,0,256);
		n = read(client_sockfd, sock_buffer, 255);
		message_received = sock_buffer;
		if (n < 0) 
		{
			perror("ERROR reading from socket");
			close(client_sockfd);
			return -4;
		}
		else 
		{
			close(client_sockfd);
			return 0;
		}
	}

}


int risikopoly_client::receive_message(string message_to_send)
{
	int client_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(client_sockfd < 0)
	{
		perror("ERROR, cannot open socket");
		fl_message("ERROR, cannot open socket");
		close(client_sockfd);
		return 1;
	}
	struct hostent *server;
	char sock_buffer[256];
	server = gethostbyname(host_ip.c_str());
	if (server == NULL) 
	{
		perror("ERROR, no such host");
		fl_message("ERROR, no such host");
		close(client_sockfd);
		return -1;
	
	}
	memset(((char *) &serv_addr), (0), (sizeof(serv_addr)));
	serv_addr.sin_family = AF_INET;
	memmove(((char *)&serv_addr.sin_addr.s_addr), ((char *)server->h_addr), (server->h_length));
	serv_addr.sin_port = htons(host_port);
	if (connect(client_sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
	{
		perror("ERROR connecting");
		close(client_sockfd);
		return 1;
	}
	memset(sock_buffer,0,256);
	int n = read(client_sockfd, sock_buffer, 255);
	message_received = sock_buffer;
	if (n < 0) 
	{
		perror("ERROR reading from socket");
		close(client_sockfd);
		return -4;
	}
	else
	{
		n = write(client_sockfd, message_to_send.c_str(), message_to_send.length());
		if (n < 0) 
		{
			
			//cout << "ERROR writing to socket\n";
			perror("ERROR writing to socket");
			close(client_sockfd);
			return -3;
		}
		
		else 
		{
			close(client_sockfd);
			return 0;
		}
	}

}


int risikopoly_client::send_all_messages()
{
	string message;
	while(list_of_messages.size() > 0)
	{
		message = list_of_messages.front();		
		list_of_messages.pop();
		//cout << "Sending Message: " << message << endl;
		int n = 1;
		while(n)
		{
			n = send_message(message.c_str()); 
			if(n < 0)
			{
				//throw(n);
			}
		}
		n_of_pending_messages = atoi(message_received.c_str());
		update_messages();
	}

}


int risikopoly_client::update_messages()
{
	int i=0, n;
	while(i < n_of_pending_messages)
	{
		n=1;

		cout << "Decoding message " << i+1 << " out of " << n_of_pending_messages << endl; 
		while(n)
		{
			n = receive_message(); 
			if(n < 0)
			{
				return n;
			}
		}
		if(!decode_instruction(message_received))
		{	
			cout<<"Failed decoding\n";
			return -6;
		}
		else
		{
			cout<<"Decoding ok\n";
			i++;
		}
	}
}

int risikopoly_client::synchronize()
{
	chrono::high_resolution_clock::time_point  now =  chrono::high_resolution_clock::now();
	chrono::duration<double> time_diff = chrono::duration_cast<chrono::duration<double>>(now- last_sync);
	if(time_diff.count() > 1.)
	{			
		send_all_messages();
		stringstream message;
		message << 0 << "," << gamer_ID;
		
		int n=1;
		
		while(n)
		{
			n = send_message(message.str().c_str()); 
			if(n < 0)
			{
				return n;
			}
		}
		n_of_pending_messages = atoi(message_received.c_str());
		if(n_of_pending_messages > 0)
		{
			cout << "You have " << n_of_pending_messages << " pending messages\n";
		}
		update_messages();	
		last_sync =  chrono::high_resolution_clock::now();
		return 1;
	}
	else
	{
		return 0;
	}
}

bool risikopoly_client::decode_instruction(string instruction)
{
	int pos = instruction.find(","), pre_pos=0;
	string temp = instruction; // = instruction.substr(0, pos + 1);
	int N_INSTR = atoi(instruction.substr(0, pos).c_str());
	
	//cout<<"Instruction received: "<< instruction<<endl;
	switch(N_INSTR)
	{
		case INIT_INFO:
			temp = instruction.substr(pos + 1);
			pos = temp.find(",");
			n_players = atoi(temp.substr(0, pos).c_str());
			for(int i=0; i<n_players; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				gamer[i].name = new char[temp.substr(0, pos).length() + 1];
				strcpy(gamer[i].name, temp.substr(0, pos).c_str());
			}
			break;
		case CLOSE:
			throw(2);
			
			break;
		case ALL_CONTRACTS:
			for(int i=0; i<N_CONTRACTS; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				contr[i].owner = atoi(temp.substr(0, pos).c_str());
			} 
			break;
		case ALL_TERRITORIES:
			for(int i=0; i<N_TERRITORIES; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				terr[i].owner = atoi(temp.substr(0, pos).c_str());
			} 
			break;
		case ALLTERRCARDS:
			for(int i=0; i<N_TERRITORY_CARDS; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				terr_card_deck.push(atoi(temp.substr(0, pos).c_str()));
			} 
			break;
		case ALLCHANCECARDS:
			for(int i=0; i<CHANCE_CARDS; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				chance_deck.push(atoi(temp.substr(0, pos).c_str()));
			} 
			break;
		case ALLCHESTCARDS:
			for(int i=0; i<CHEST_CARDS; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				chest_deck.push(atoi(temp.substr(0, pos).c_str()));
			} 
			new_game = true;
			break;
		case AUCTION:
		{	
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int offerer;
			offerer = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int offer;
			offer = atoi(temp.substr(0, pos).c_str());
			stringstream summary_message;
			if(offer == -1)
			{
				N_offerers --;
				if(offerer == gamer_ID)
				{
					summary_message << "You abandoned. ";
				}
				else
				{
					summary_message << gamer[offerer].name <<" abandoned. ";
				}
				summary_message << "\nRemaining offerers: " << N_offerers << endl;
			}
			else	
			{
				current_offer[0] = offer;
				current_offer[1] = offerer;
				if(offerer == gamer_ID)
				{
					summary_message << "You offered "<< current_offer[0] << " citytanki\n";
				}
				else
				{
					summary_message << gamer[offerer].name <<" offered "<< current_offer[0] << " citytanki\n";
				}
				
			}
			write_info_message(summary_message.str(), (offerer == gamer_ID ? false : true));
			update_info_text();
			break;
		}
		case NEXT_TURN:
		{
			atk_value[0] = atk_value[1] = atk_value[2] = def_value[0] = def_value[1] = def_value[2] = 0;
			attack_successfull = false;
			numberOfMove = 0;
			draw_dice();
			for(int i=0; i<N_TERRITORIES; i++)
			{
				if(terr[i].owner < n_players)
				{
					decode_territory_name(i, gamer[terr[i].owner].color);
				}
				else
				{
					decode_territory_name(i, EMPTY);
				}
			}		

			for(int i=0; i<3; i++)
			{
				atk_value[i] = 0;
				def_value[i] = 0;
			}
			draw_dice();

			player_turn ++; 
			if(player_turn % n_players == gamer_ID)
			{
				write_info_message("YOUR TURN", true);
			}
			else
			{
				stringstream summary_message;
				summary_message << gamer[player_turn % n_players].name << "'s turn\n";
				write_info_message(summary_message.str(), false);
			}
			gamer[player_turn % n_players].money += find_init_money(player_turn % n_players) * 15000;
			save();
			break;
		}
		case LOAD:
		{
			load();
			break;
		}
		case NOTHING:
		{
			break;
		}
		case SELECT_ATTACKER:
		{
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int gamer_attacker = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			attacker = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			attacked = atoi(temp.substr(0, pos).c_str()); 
			stringstream summary_message;
			if(gamer_attacker == gamer_ID)
			{
				summary_message << "You attacked "<< gamer[terr[attacked].owner].name << endl;
			}
			else if(terr[attacked].owner == gamer_ID)
			{
				
				fl_message_title("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
				summary_message << "You have been attacked by "<< gamer[terr[attacker].owner].name << endl;
			}
			else
			{
				summary_message << gamer[terr[attacker].owner].name << " attacked "<< gamer[terr[attacked].owner].name << endl;
			}
			summary_message << terr[attacker].name << " VS " << terr[attacked].name << endl;
			write_info_message(summary_message.str(), true);
			decode_territory_name(attacker, WHITE);
			decode_territory_name(attacked, WHITE);
			if(terr[attacked].owner == gamer_ID)
			{
				//select_armies();
				under_attack = true;
			}
			break;
		}
		case SELECT_ARMIES:
		{
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int gamer_sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int atk = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			if(atk == 0)
			{
				armies_def = atoi(temp.substr(0, pos).c_str()); 
			}
			else
			{
				armies_atk = atoi(temp.substr(0, pos).c_str()); 
			}
			break;
		}
		case DICE_FIGHT:
		{
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int gamer_sender = atoi(temp.substr(0, pos).c_str());
			for(int i=0; i<3; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				atk_value[i] = atoi(temp.substr(0, pos).c_str());
			}
			for(int i=0; i<3; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				def_value[i] = atoi(temp.substr(0, pos).c_str());
			}
			for(int i=0; i<3; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				yellow_die[i] = atoi(temp.substr(0, pos).c_str());
			}
			draw_dice();
			armies_atk_lost = 0;
			int armies_def_lost = 0, gamer_attacker, gamer_attacked;
			gamer_attacker = terr[attacker].owner;
			gamer_attacked = terr[attacked].owner;
			for(int i=0; i<min(armies_atk, armies_def); i++)
			{
				if(atk_value[i] > def_value[i])
				{
					if(yellow_die[i])
					{
						terr[attacked].n_defensive_armies--;
					}
					else
					{
						terr[attacked].n_offensive_armies--;
					}
					armies_def_lost ++;
				}
				else if(atk_value[i] <= def_value[i])
				{
					terr[attacker].n_offensive_armies--;
					armies_atk_lost ++;
				}
				/*else if(atk_value[i] == def_value[i] && yellow_die[i])
				{
					terr[attacker].n_offensive_armies--;
					armies_atk_lost ++;
				}*/
			}
			stringstream summary_message;
			summary_message << "ATTACKER: ";
			if(gamer_attacker == gamer_ID)
			{
				summary_message << "You";
			}
			else
			{
				summary_message << gamer[gamer_attacker].name;
			}
			summary_message << " lost " << armies_atk_lost << " armies; left: " << terr[attacker].n_offensive_armies << endl;
			summary_message << "DEFENDER: ";
			if(gamer_attacked == gamer_ID)
			{
				summary_message << "You";
			}
			else
			{
				summary_message << gamer[gamer_attacked].name;
			}
			summary_message << " lost " << (armies_def_lost < armies_def ? armies_def_lost : armies_def) << " armies; left: "; 
			summary_message << (terr[attacked].n_offensive_armies > 0 ? terr[attacked].n_offensive_armies : 0) << endl;
			write_info_message(summary_message.str(), true);
			for(int i=0; i<3; i++)
			{
				atk_value[i] = def_value[i] = 0;
				yellow_die[i] = 0;
			}
			break;
		}
		case CONQUER_TERR:
		{
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int gamer_sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int n_terr = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			for(int i=0; i<3; i++)
			{
				atk_value[i] = 0;
				def_value[i] = 0;
			}
			draw_dice();
			terr[attacked].owner = gamer_sender;
			terr[attacker].n_offensive_armies -= n_terr;			
			terr[attacked].n_offensive_armies = n_terr;
			terr[attacked].n_defensive_armies = 0; 

			stringstream summary_message;
			summary_message << (gamer_sender == gamer_ID ? "You" : gamer[gamer_sender].name) << " conquered " << terr[attacked].name << " with " << terr[attacker].name;
			summary_message << ".\n He moved " << n_terr << " tanks.\n";
			summary_message << terr[attacker].name << " now has " << terr[attacker].n_offensive_armies << " tanks in total\n";
			summary_message << terr[attacked].name << " now has " << terr[attacked].n_offensive_armies << " tanks in total\n";
			write_info_message(summary_message.str(), (gamer_sender == gamer_ID ? false : true));
			decode_territory_name(attacker, gamer[gamer_sender].color);
			decode_territory_name(attacked, gamer[gamer_sender].color);
			break;
		}
		case MOVEMENT:
		{
			short int dice_value[2];
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int gamer_sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			dice_value[0] = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			dice_value[1] = atoi(temp.substr(0, pos).c_str());			
		
			draw_dice(dice_value[0], dice_value[1]);
			int position_prev = gamer[gamer_sender].position;
			gamer[gamer_sender].position = (gamer[gamer_sender].position + dice_value[0] + dice_value[1]) % 40;		
			decode_pawn_name(gamer_sender);
			if((position_prev + dice_value[0] + dice_value[1]) >= 40)
			{
				gamer[gamer_sender].money += 20000;
				first_start_passed[gamer_sender] = true;
				if (gamer_sender == gamer_ID)
				{
					fl_message("You passed through the start, you get 20000 ctk!");
				}
			}
			proceed_destination(gamer_sender);
			process_stations();
			break;
		}
		case EXCHANGE:
		{
			for(int i=0; i<N_CONTRACTS; i++)
			{
				offered_items[4+i] = 100;
				desidered_items[4+i] = 100;
			}
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			exchange_offerer = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			exchange_receiver = atoi(temp.substr(0, pos).c_str());
			if(exchange_receiver == gamer_ID)
			{
				exchange_happened = true;
			}
			for(int i=0; i<4; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				offered_items[i] = atoi(temp.substr(0, pos).c_str());
			}
			while(1)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				int read_value = atoi(temp.substr(0, pos).c_str());
				if(read_value == END_OF_TRASMISSION)
				{
					break;
				}
				else
				{
					offered_items[read_value + 4] = 1;
				}
			}
			for(int i=0; i<4; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				desidered_items[i] = atoi(temp.substr(0, pos).c_str());
			}
			while(1)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				int read_value = atoi(temp.substr(0, pos).c_str());
				if(read_value == END_OF_TRASMISSION)
				{
					break;
				}
				else
				{
					desidered_items[read_value + 4] = 1;
				}
			}
			break;
		}
		case OFFER:
		{
			from_offer = true;
			for(int i=0; i<N_CONTRACTS; i++)
			{
				offered_items[4+i] = 100;
				desidered_items[4+i] = 100;
			}
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			exchange_offerer = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			exchange_receiver = atoi(temp.substr(0, pos).c_str());
			for(int i=0; i<4; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				offered_items[i] = atoi(temp.substr(0, pos).c_str());
				desidered_items[i] = 0;
			}
			while(1)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				int read_value = atoi(temp.substr(0, pos).c_str());
				if(read_value == END_OF_TRASMISSION)
				{
					break;
				}
				else
				{
					offered_items[read_value + 4] = 1;
				}
			}
			
			if(exchange_receiver == gamer_ID)
			{
				try
				{
					list_of_messages.push(make_offer(exchange_offerer, exchange_receiver, OFFER, true));
				}
				catch(int i)
				{
					stringstream MSG;
					MSG << DECIDE_EXCHANGE << "," << gamer_ID << "," << 0;
					list_of_messages.push(MSG.str());
				}
			}
			break;
		}
		case DECIDE_EXCHANGE:
		{
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int gamer_sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int answer = atoi(temp.substr(0, pos).c_str());
			if(answer == 1)
			{
				stringstream summary_message;
				summary_message << "Exchange between ";
				summary_message << (exchange_offerer == gamer_ID ? "you" : gamer[exchange_offerer].name) << " and ";
				summary_message << (exchange_receiver == gamer_ID ? "you" : gamer[exchange_receiver].name) << endl;
				gamer[exchange_offerer].money = gamer[exchange_offerer].money - offered_items[0] + desidered_items[0];
				gamer[exchange_receiver].money = gamer[exchange_receiver].money + offered_items[0] - desidered_items[0];

				summary_message << (exchange_offerer == gamer_ID ? "You" : gamer[exchange_offerer].name) << " gave:\n";
				summary_message << offered_items[0] << " citytanks\n";
				for(int i=0; i<N_CONTRACTS; i++)
				{
					if(offered_items[4+i] == 1)
					{
						contr[i].owner = exchange_receiver;
						decode_contract_btn(i);
						summary_message << contr[i].name << " \n";
					}
				}
				summary_message << (exchange_receiver == gamer_ID ? "You" : gamer[exchange_receiver].name) << " gave:\n";
				summary_message << desidered_items[0] << " citytanks\n";
				for(int i=0; i<N_CONTRACTS; i++)
				{
					if(desidered_items[4+i] == 1)
					{
						contr[i].owner = exchange_offerer;			
						decode_contract_btn(i);
						summary_message << contr[i].name << " \n";
					}
				}
				if(gamer_ID == exchange_offerer)
				{
					fl_message("Offer accepted!");
					if(from_offer)
					{
						payment_needed = false;
					}
					/*else if(payment_needed)
					{
						stringstream message;
						message << PAY << "," << gamer_ID;
						list_of_messages.push(message.str());
					}*/
					
					write_info_message(summary_message.str(), false);
				}
				else if(gamer_ID == exchange_receiver)
				{
					write_info_message(summary_message.str(), false);
				}
				else
				{
					write_info_message(summary_message.str(), true);
				}
			}
			else
			{
				if(gamer_ID == exchange_offerer)
				{
					if(answer == 2)
					{
						fl_message("The offer is not valid anymore");
					}
					else
					{
						fl_message("Offer refused!");
					}
					if(from_offer == true)
					{
						stringstream message;
						message << PAY << "," << gamer_ID;
						list_of_messages.push(message.str());
					}
				}
			}
			process_stations();
			from_offer = false;
			break;
		}
		case BUY_HOUSE: // 0=buy, 1=sell, 2=mortgage, 3=demortgage
		{
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int gamer_sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int contr_number = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			short int value = atoi(temp.substr(0, pos).c_str());			
			
			string info_message;
			stringstream summary_message;
			if(gamer_sender == gamer_ID)
			{
				summary_message << "You";
			}
			else
			{
				summary_message << gamer[gamer_sender].name;
			}

			if(value == 0)
			{
				gamer[gamer_sender].money -= contr[contr_number].price_house;
				contr[contr_number].n_houses ++;
				summary_message << " built a house in " << contr[contr_number].name.c_str() << "\n";
			}
			else if(value == 1)
			{
				if(contr[contr_number].n_houses < 5)
				{
					gamer[gamer_sender].money += (contr[contr_number].price_house/2);
					contr[contr_number].n_houses --;
					summary_message << " sold a house in " << contr[contr_number].name.c_str() << "\n";
				}
				else
				{
					gamer[gamer_sender].money += (contr[contr_number].price_house/2*5);
					contr[contr_number].n_houses = 0;
					summary_message << " sold a hotel in " << contr[contr_number].name.c_str() << "\n";	
				}
			}
			else if(value == 2)
			{
				gamer[gamer_sender].money += (contr[contr_number].price/2);
				contr[contr_number].mortgage = true;			
				summary_message << " mortgaged " << contr[contr_number].name.c_str() << "\n";
			}
			else if(value == 3)
			{
				gamer[gamer_sender].money -= (contr[contr_number].price/2 + contr[contr_number].price/10);
				contr[contr_number].mortgage = false;			
				summary_message << " de-mortgaged " << contr[contr_number].name.c_str() << "\n";
			}
			write_info_message(summary_message.str(), true);

			draw_houses();
			break;
		}
		case PAY:
		{
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int gamer_sender = atoi(temp.substr(0, pos).c_str());
			stringstream info_string, message;
			if(gamer_sender == gamer_ID)
			{
				payment();
			}
			break;
		}
		case PAYMENT:
		{
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int receiver = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int money = atoi(temp.substr(0, pos).c_str());
	
			stringstream summary_message;
			summary_message << (gamer_ID == sender ? "You" : gamer[sender].name) << " paid ";
			summary_message << money << " ctk";
			if(receiver != 100)
			{
				summary_message << " to " << (gamer_ID == receiver ? "you" : gamer[receiver].name) << endl ;
				gamer[receiver].money += money;
			}
			gamer[sender].money -= money;
			write_info_message(summary_message.str(), true);

			payment_needed = false;
			break;

		}
		case BUY_OR_AUCTION:
		{
			stringstream info_string, message, summary_message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int contr_number = atoi(temp.substr(0, pos).c_str());
			int price = contr[contr_number].price;
			info_string << "You stepped into " << contr[contr_number].name << ". Do you want to buy it?";
			summary_message << (sender == gamer_ID ? "You" : gamer[sender].name) << " stepped into " << contr[contr_number].name << " which has no owner yet.\n";
			if(sender == gamer_ID)
			{
				write_info_message(summary_message.str(), false);
				if(price > gamer[gamer_ID].money)
				{
					int result = fl_choice("%s", "Auction", 0, 0, info_string.str().c_str());
					if(result == 0)
					{
						message << NEW_AUCTION << "," << gamer_ID << "," << contr_number;
						list_of_messages.push(message.str()); 
					}
				}
				else
				{	
					int result = fl_choice("%s", "Auction", "Buy", 0, info_string.str().c_str()) ;
					if(result == 0)
					{
						message << NEW_AUCTION << "," << gamer_ID << "," << contr_number;
						list_of_messages.push(message.str()); 
					}
					else if(result == 1)
					{
						message << BUY_CONTRACT << "," << gamer_ID << "," << contr_number;
						list_of_messages.push(message.str()); 
					}
				}
			}
			else
			{
				write_info_message(summary_message.str(), true);
			}
			break;

		}
		case BUY_CONTRACT:
		{
			stringstream info_string, message, summary_message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int contr_number = atoi(temp.substr(0, pos).c_str());
			int price = contr[contr_number].price;
			contr[contr_number].owner = sender;
			gamer[sender].money -= price;
			process_stations();
			const char *subject = (sender == gamer_ID ? "You" : gamer[sender].name);
			summary_message << subject << " bought " << contr[contr_number].name << ".\n" << subject << " payed " << price << " ctk";
			write_info_message(summary_message.str(), true);
			break;

		}
		case NEW_AUCTION:
		{
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int contr_number = atoi(temp.substr(0, pos).c_str());
			stringstream summary_message;
			summary_message << " Auction for contract " << contr[contr_number].name << endl;
			summary_message << "Original Price : " << contr[contr_number].price << endl;
			write_info_message(summary_message.str(), true);
			contr[contr_number].owner = auction();
			process_stations();
			break;
		}
		case SELLTANK:
		{
			stringstream info_string, message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int terr_number = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int n_tanks = atoi(temp.substr(0, pos).c_str());
			if(terr[terr_number].owner == sender && terr[terr_number].n_offensive_armies > n_tanks)
			{
				terr[terr_number].n_offensive_armies -= n_tanks;
			}
			break;
		}
		case BUYTANK:
		{
			stringstream summary_message, message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int n_tanks = atoi(temp.substr(0, pos).c_str());
			if(gamer[sender].money >= n_tanks*15000)
			{
				gamer[sender].money -= n_tanks*15000;
				summary_message << (sender == gamer_ID ? "You" : gamer[sender].name);
				summary_message << " bought " << n_tanks << " tanks.\n Paid " << n_tanks*15000 << " citytank\n";
				if(n_tanks > 0)
				{
					write_info_message(summary_message.str(), true);
					if(sender == gamer_ID)
					{
						dispose_tanks(n_tanks);
					}
				}
			}
			else if(sender == gamer_ID)
			{
				summary_message << "You wanted to buy " <<  n_tanks << " tanks but you don't have enough money. Please try again";
				fl_alert("%s", summary_message.str().c_str());
				buy_tanks();
			}
			break;
		}
		case DISPOSETANK:
		{
			stringstream summary_message, message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			summary_message << (sender == gamer_ID ? "You" : gamer[sender].name) << " disposed:\n";
			for(int i=0; i<N_TERRITORIES; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				int n_tanks = atoi(temp.substr(0, pos).c_str());
				if(n_tanks > 0)
				{
					terr[i].n_offensive_armies += n_tanks;
					summary_message << n_tanks << " tanks in " << terr[i].name << ". Tot tanks :" << terr[i].n_offensive_armies << endl;	
				}
			}
			write_info_message(summary_message.str(), (sender == gamer_ID ? false : true));

			break;
		}
		case REMOVETANK:
		{
			stringstream summary_message, message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			summary_message << (sender == gamer_ID ? "You" : gamer[sender].name) << " removed:\n";
			for(int i=0; i<N_TERRITORIES; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				int n_tanks = atoi(temp.substr(0, pos).c_str());
				if(n_tanks > 0)
				{
					terr[i].n_offensive_armies -= n_tanks;
					summary_message << n_tanks << " tanks from " << terr[i].name << " \n\t(owner = " << (terr[i].owner == gamer_ID ? "You" : gamer[terr[i].owner].name) << ")" << ".\n \tTot tanks :" << terr[i].n_offensive_armies << endl;	
				}
			}
			write_info_message(summary_message.str(), (sender == gamer_ID ? false : true));

			break;
		}
		case MOVETANK:
		{
			stringstream summary_message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int donor = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int receiver = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int n_tanks = atoi(temp.substr(0, pos).c_str());
			if(terr[donor].owner == sender && terr[receiver].owner == sender && terr[donor].n_offensive_armies > n_tanks)
			{
				summary_message << (sender == gamer_ID ? "You" : gamer[sender].name) << " moved ";
				summary_message << n_tanks << " tanks from " << terr[donor].name << " to " << terr[receiver].name << endl;	
				write_info_message(summary_message.str(), (sender == gamer_ID ? false : true));
				terr[donor].n_offensive_armies -= n_tanks;
				terr[receiver].n_offensive_armies += n_tanks;
			}
			else
			{
				summary_message << (sender == gamer_ID ? "You" : gamer[sender].name) << " wanted to move ";
				summary_message << n_tanks << " tanks from " << terr[donor].name << " to " << terr[receiver].name << endl;	
				summary_message << "Something went wrong! Aborting...\n";
				write_info_message(summary_message.str(), (sender == gamer_ID ? true : false));
			}
			break;
		}
		case CHANGE_TERR_CARDS:
		{
			stringstream summary_message;
			int card_changed[3];
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int money_back = atoi(temp.substr(0, pos).c_str());
			for(int i=0; i<3; i++)
			{
				temp = temp.substr(pos + 1);
				pos = temp.find(",");
				card_changed[i] = atoi(temp.substr(0, pos).c_str());
			}
			decode_terr_card();

			summary_message << (sender==gamer_ID ? "You" : gamer[sender].name);
			int can_changed=0, ped_changed=0, horse_changed=0, jolly_changed=0;
			if(terr_card[card_changed[0]].owner == sender && terr_card[card_changed[1]].owner == sender && terr_card[card_changed[2]].owner == sender)
			{
				for(int i=0; i<3; i++)
				{
					terr_card[card_changed[i]].owner = 100;
					terr_card_deck.push(card_changed[i]);
					gamer[sender].money += money_back * 15000;
					if(card_changed[i] < N_TERRITORIES/3)
					{
						can_changed ++;
					}
					else if(card_changed[i] < N_TERRITORIES/3*2)
					{
						ped_changed ++;
					}
					else if(card_changed[i] < N_TERRITORIES)
					{
						horse_changed ++;
					}
					else
					{
						jolly_changed ++;
					}
				}
				summary_message << " changed " << can_changed << " cannons, "  << ped_changed << " pedestrians, "  << horse_changed << " horses and "  << jolly_changed << " jollies.\n Received " << money_back*15000 << " ctk.\n";
				write_info_message(summary_message.str(), true);				
				decode_terr_card();
			}
			else
			{
				summary_message << " wanted to change territory cards but an error occurred, nothing happened\n";
				fl_alert("%s", summary_message.str().c_str());
			}
			break;

		}
		case WIN_TERR_CARD:
		{
			stringstream summary_message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			int card_won = terr_card_deck.front();
			terr_card_deck.pop();
			const char *card_type[4]={"CANNON", "PEDESTRIAN", "HORSE", "HORSE"};
			int card_divider = N_TERRITORIES/3;
			if(terr_card_deck.size() > 0)
			{
				terr_card[card_won].owner = sender;
				if(sender == gamer_ID)
				{
					if(card_won < N_TERRITORIES)
					{
						summary_message << "You picked the territory card " << terr[card_won].name << ": " << card_type[card_won/card_divider] << endl;
					}
					else
					{
						summary_message << "You picked a JOLLY!\n";
					}
				}
				else
				{
					summary_message << gamer[sender].name << " won a territory card!\n";
				}
				write_info_message(summary_message.str(), true);
			}
			else
			{
				if(sender == gamer_ID)
				{
					fl_alert("Sorry, there are no more territory cards for you\n");
				}
			}
			break;
		}

		case PRISON:
		{
			stringstream summary_message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int die0 = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int die1 = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int prison_turn = atoi(temp.substr(0, pos).c_str());

			const char *subject = (sender == gamer_ID ? "You" : gamer[sender].name);
			summary_message << subject << " rolled from prison: " << die0 << " and " << die1 <<endl;
			if(die0 == die1)
			{
				gamer[sender].state = 0;
				summary_message << subject << " exited from prison!\n";
				write_info_message(summary_message.str(), true);
			}
			else if(prison_turn <= 3)
			{
				summary_message << subject << " failed to exit from prison. Attempt number " << prison_turn+1 <<endl;
				write_info_message(summary_message.str(), true);
				turnsInPrison[sender] ++;
			}
			else
			{
				gamer[sender].state = 0;
				summary_message << subject << " failed to exit from prison 3 times.\n" << subject << " must pay 15000 ctk.\n" <<endl;
				write_info_message(summary_message.str(), true);
				turnsInPrison[sender] = 0;
				if(sender == gamer_ID)
				{
					payment(false, 15000);
				}
			}
			break;

		}
		case GOTOPRISON:
		{
			stringstream summary_message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int die0 = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int die1 = atoi(temp.substr(0, pos).c_str());

			gamer[sender].position = 10;
			decode_pawn_name(sender);

			gamer[sender].state = 2;
			const char *subject = (sender == gamer_ID ? "You" : gamer[sender].name);
			summary_message << subject << " rolled "<< die0 << " and " << die1 << ":\n3 times equal dice\n" << subject << " must go to prison\n";
			write_info_message(summary_message.str(), true);
			break;
		}
		case PRISON_EXIT:
		{
			stringstream summary_message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int isChest = atoi(temp.substr(0, pos).c_str());

			const char *subject = (sender == gamer_ID ? "You" : gamer[sender].name);
			summary_message << subject << " used the card 'Exit from Prison'\n";
			write_info_message(summary_message.str(), true);

			gamer[sender].state = 0;
			turnsInPrison[sender] = 0;
			if(isChest)
			{
				chest_deck.push(11);
			}
			else
			{
				chance_deck.push(15);
			}
			break;
		}
		case PAY_FOR_PRISON:
		{
			stringstream summary_message, message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			temp = temp.substr(pos + 1);
			pos = temp.find(",");

			const char *subject = (sender == gamer_ID ? "You" : gamer[sender].name);
			summary_message << subject << " decided to pay 15000 ctk to exit from prison\n";
			write_info_message(summary_message.str(), (sender == gamer_ID ? false : true));
			gamer[sender].state = 0;
			turnsInPrison[sender] = 0;
			if(sender == gamer_ID)
			{
				payment(false, 15000);
				movement();
			}
			break;
		}

		case DRAW_CHANCE:
		{
			stringstream summary_message, message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			const char *subject = (sender == gamer_ID ? "You" : gamer[sender].name);
			summary_message << subject << " decided to take the risk and draw a chance card\n";
			write_info_message(summary_message.str(), (sender == gamer_ID ? false : true));
			summary_message.str("");
			int chance_picked = chest_deck.front();
			chance_deck.pop();

			summary_message << chance_name[chance_picked] << endl;	
			write_info_message(summary_message.str(), true);
			decode_chance(sender, chance_picked);
			break;
		}

		case PAY_TICKET:
		{
			stringstream summary_message, message;
			temp = temp.substr(pos + 1);
			pos = temp.find(",");
			int sender = atoi(temp.substr(0, pos).c_str());
			const char *subject = (sender == gamer_ID ? "You" : gamer[sender].name);
			summary_message << subject << " decided to pay the ticket\n";
			write_info_message(summary_message.str(), (sender == gamer_ID ? false : true));
			if(sender == gamer_ID)
			{
				//payment_needed = true;
				payment(false, 15000);
			}
			break;
		}

		default: 
			allegro_message("Error, Instruction not recognized\n"); 
			return false;	
	}

	update_info_text();
	return true;
}

