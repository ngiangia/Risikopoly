#include "risikopoly_client.h"

void help_callback(Fl_Widget *widget, void *)
{
	fl_message("Congratulation for pressing the \"Help\" button");
	fl_message("You lost 10'000 ctk");
	throw(LOSE_MONEY_CB);

}


Fl_Menu_Item menu_[] = 
{
	{"File", 0,  0, 0, 192, FL_ENGRAVED_LABEL, 0, 14, 0},
	{"Save", 0,  0, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
	{"item", 0,  0, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
	{0,0,0,0,0,0,0,0,0},
	{"Edit", 0,  0, 0, 192, FL_ENGRAVED_LABEL, 0, 14, 0},
	{"What do you want to edit?\?\?", 0,  0, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
	{0,0,0,0,0,0,0,0,0},
	{"Help", 0,  0, 0, 192, FL_ENGRAVED_LABEL, 0, 14, 0},
	{"Press for Help", 0,  0, 0, 0, FL_NORMAL_LABEL, 0, 14, 0},
	{0,0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,0,0,0}
};


void risikopoly_client::check_buttons()
{
	process_stations();
	send_all_messages();
	if(exchange_happened)
	{
		exchange_happened = false;
		string message;
		try
		{
			message = make_offer(exchange_offerer, exchange_receiver, EXCHANGE, true);
		}
		catch(int i)
		{
			stringstream MSG;
			MSG << DECIDE_EXCHANGE << "," << gamer_ID << "," << 0;
			message = MSG.str();
		}
		list_of_messages.push(message);
		send_all_messages();

		exchange_happened = false;
	}
	if(audio_select->changed())
	{
		//intro_audio_select->clear();
		//cout << "Ciaooo\n";
		if(audio_select->value())
		{
			play_sample(main_theme, 255,128,1000, TRUE);
			audio_select->clear();
			audio_select->value(1);
		}
		else
		{
			stop_sample(main_theme);
			audio_select->clear();
		}
	}
	if(fighting == false && disposing_tanks == false)
	{
		for(int i=0; i<N_TERRITORIES + 1; i++)
		{
			if(state_btn[i]->changed())
			{			
				if(selected_state < N_TERRITORIES)
				{
					decode_territory_name(selected_state, terr[selected_state].owner < 100 ? gamer[terr[selected_state].owner].color : EMPTY);
				}
				selected_state = i;
				state_btn[i]->clear();
				if(selected_state < N_TERRITORIES)
				{
					//stringstream army_text;
					//army_text << terr[i].name <<"\nOff: " << terr[i].n_offensive_armies << "\nDef: "<< terr[i].n_defensive_armies<<endl;
					decode_territory_name(i, WHITE);
					//army_display->show();
					//army_text_buff->text(army_text.str().c_str());
					sell_tanks(selected_state);
				}
				else
				{
					army_display->hide();
				}
				break;	
			}
		}

	}

	if(window == 1)
	{
		for(int i=0; i<N_CONTRACTS; i++)
		{
			if(contract_btn[i]->changed())
			{			
				contract_btn[i]->clear();
				house_construction(i);	
			}
		}

		for(int i=0; i<n_players; i++)
		{
			if(terr_card_btn[i]->changed())
			{			
				terr_card_btn[i]->clear();
				terr_card_view(i);	
			}
		}
		for(int i=0; i<2*n_players; i++)
		{
			if(pri_exit_btn[i]->changed())
			{			
				pri_exit_btn[i]->clear();
				stringstream message;
				if(gamer[gamer_ID].state == 2 && i/2 == gamer_ID)
				{
					int choice = fl_choice("Exit from Prison\nDo you wanto to use it?", "No", "Yes", 0);
					if(choice == 1)
					{
						message << PRISON_EXIT << "," << gamer_ID << "," << i%2;
						list_of_messages.push(message.str());
					}
				}	
				else
				{
					fl_message("Exit from Prison\n");
				}
			}
		}
	}
	
	if(exchange_btn -> changed())
	{
		exchange_btn -> clear();
		stringstream message;
		short int receiver = choose_player();
		if(receiver < n_players)
		{
			try
			{
				message << make_offer(gamer_ID, receiver, EXCHANGE);
				int n = 1;
				list_of_messages.push(message.str());
				send_all_messages();		
			}
			catch(int i)
			{
			}
		}
	}

	if(change_view_btn -> changed())
	{
		change_view_btn->clear();
		decode_terr_card_btn();
		decode_pri_exit_btn();
		if(window == 0)
		{
			window = 1;
			bg_second_box->show();
			for(int i=0; i<N_CONTRACTS; i++)
			{
				decode_contract_btn(i);
			}
			offer_btn->deactivate();
			not_interested_btn->deactivate();
			yes_btn->deactivate();
			no_btn->deactivate();
			ok_btn->deactivate();
			info_display->hide();
			main_display->hide();
			for(int i=0; i<45; i++)
			{
				state_btn[i]->hide();
			}
		}
		else
		{				
			window = 0;	
			bg_second_box->hide();
			for(int i=0; i<N_CONTRACTS; i++)
			{
				contract_btn[i]->hide();
			}
			offer_btn->activate();
			not_interested_btn->activate();
			yes_btn->activate();
			no_btn->activate();
			ok_btn->activate();				
			info_display->show();
			main_display->show();
			for(int i=0; i<45; i++)
			{
				state_btn[i]->show();
			}
		}	
		draw_houses();
	}
}



// This window callback allows the user to save & exit, don't save, or cancel.
static void window_cb (Fl_Widget *widget, void *) 
{
	Fl_Window *window = (Fl_Window *)widget;
	fl_message_title("Nooooooooooo =(");
	// fl_choice presents a modal dialog window with up to three choices.
	int result = fl_choice("Do you feel brave enough to exit?", "Cancel", "Quit", 0);
	if (result == 1) 
	{  // Save and close
		//save();
		window->hide();
		throw(CLOSE_CB);
	} 

}

// This window callback allows the user to save & exit, don't save, or cancel.
static void window_cb2 (Fl_Widget *widget, void *) 
{
	Fl_Window *window = (Fl_Window *)widget;	
	window->hide();
	throw(3);
}

// This window callback allows the user to save & exit, don't save, or cancel.
static void window_cb3 (Fl_Widget *widget, void *) 
{
	Fl_Window *window = (Fl_Window *)widget;
	fl_message_title("Nooooooooooo =(");
	// fl_choice presents a modal dialog window with up to three choices.
	int result = fl_choice("Do you feel brave enough to exit?", "Cancel", "Quit", 0);
	if (result == 1) 
	{  // Save and close
		//save();
		window->hide();
		throw(3);
	} 
}


void risikopoly_client::graphics_init()
{
	main_screen			= new Fl_Double_Window(1200, 705);	
	//visible only in board window
	bg_main_image		= new Fl_JPEG_Image("Images//Risikopoly_board.jpg");
	bg_second_image		= new Fl_JPEG_Image("Images//Board_2.jpg");
	bg_main_box 		= new Fl_Box(0, 10, 1200, 700);
	main_display 		= new Fl_Text_Display(265, 30, 615, 80);
	info_display 		= new Fl_Text_Display(5, 127, 260, 543);
	army_display		= new Fl_Text_Display(950, 35, 175, 75);
	offer_btn 			= new Fl_Button(890, 210, 145, 35, "OFFER");
	not_interested_btn 	= new Fl_Button(1045, 210, 145, 35, "NOT INTERESTED");
	yes_btn 			= new Fl_Button(890, 210, 145, 35, "YES");
	no_btn			 	= new Fl_Button(1045, 210, 145, 35, "NO");
	ok_btn 				= new Fl_Button(920, 210, 145, 35, "OK");
	offer_input 		= new Fl_Float_Input(935, 169, 205, 31);
	main_text_buff 		= new Fl_Text_Buffer();
	info_text_buff 		= new Fl_Text_Buffer();
	army_text_buff 		= new Fl_Text_Buffer();
	
	for(int i=0; i< N_TERRITORIES; i++)
	{
		territory_box[i] = new Fl_Box(0, 60, 1200, 700);  
		territory_image[i] = new Fl_GIF_Image("Images//Territories//empty.gif");
	}	

	for(int i=0; i< 6; i++)
	{
		dice_box[i] = new Fl_Box(950 + 80*(i/3), 300 + 50*(i%3), 30, 30);  
		dice_image[i] = new Fl_GIF_Image("Images//Dice//empty.gif");
		pawn_box[i] = new Fl_Box(200 , 200, 24, 24);
		pawn_image[i] = new Fl_GIF_Image("Images//empty.gif");
	}	

	for(int i=0; i<N_CONTRACTS - 6; i++)
	{
		int position = contract_position[i];
		if(position < 10)
		{
			for(int j=0; j<4; j++)
			{
				house_box[i*5 + j] = new Fl_Box(856 - position * 54 - j* 12 , 653, 10, 10);
			}
			house_box[i*5 + 4] = new Fl_Box(835 - position * 54 , 653, 20, 10);
		}
		else if(position < 20)
		{
			for(int j=0; j<4; j++)
			{
				house_box[i*5 + j] = new Fl_Box(321, 692 - (position - 10) * 54 - j* 12 , 10, 10);
			}
			house_box[i*5 + 4] = new Fl_Box(321, 670 - (position - 10) * 54 , 10, 20);
		}
		else if(position < 30)
		{
			for(int j=0; j<4; j++)
			{
				house_box[i*5 + j] = new Fl_Box(278 + (position - 20) * 54 + j* 12 , 155, 10, 10);
			}
			house_box[i*5 + 4] = new Fl_Box(292 + (position - 20) * 54 , 155, 20, 10);
		}
		else
		{
			for(int j=0; j<4; j++)
			{
				house_box[i*5 + j] = new Fl_Box(816, 114 + (position - 30) * 54 + j* 12 , 10, 10);
			}
			house_box[i*5 + 4] = new Fl_Box(816, 125 + (position - 30) * 54 , 10, 20);
		}
	}
	
	state_btn[44] 		= new Fl_Button(330, 165, 485, 490, "");
	state_btn[0] 		= new Fl_Button(450, 180, 70, 90, "");
	state_btn[1] 		= new Fl_Button(395, 200, 45, 70, "");
	state_btn[2] 		= new Fl_Button(360, 200, 30, 60, "");
	state_btn[3] 		= new Fl_Button(365, 265, 45, 50, "");
	state_btn[4] 		= new Fl_Button(350, 315, 55, 40, "");
	state_btn[5] 		= new Fl_Button(350, 360, 35, 75, "");
	state_btn[6] 		= new Fl_Button(385, 355, 85, 30, "");
	state_btn[7] 		= new Fl_Button(410, 290, 40, 55, "");
	state_btn[8] 		= new Fl_Button(445, 295, 40, 60, "");
	state_btn[9] 		= new Fl_Button(350, 430, 95, 35, "");
	state_btn[10] 		= new Fl_Button(350, 495, 60, 40, "");
	state_btn[11] 		= new Fl_Button(370, 540, 50, 90, "");
	state_btn[12] 		= new Fl_Button(395, 450, 65, 90, "");
	state_btn[13] 		= new Fl_Button(730, 465, 50, 55, "");
	state_btn[14] 		= new Fl_Button(780, 500, 30, 40, "");
	state_btn[15] 		= new Fl_Button(715, 540, 50, 70, "");
	state_btn[16] 		= new Fl_Button(765, 545, 40, 95, "");
	state_btn[17] 		= new Fl_Button(520, 235, 40, 40, "");
	state_btn[18] 		= new Fl_Button(505, 285, 40, 65, "");
	state_btn[19] 		= new Fl_Button(555, 255, 35, 65, "");
	state_btn[20] 		= new Fl_Button(590, 265, 35, 120, "");
	state_btn[21] 		= new Fl_Button(540, 325, 45, 40, "");
	state_btn[22] 		= new Fl_Button(485, 350, 55, 65, "");
	state_btn[23] 		= new Fl_Button(540, 360, 55, 70, "");
	state_btn[24] 		= new Fl_Button(500, 425, 50, 90, "");
	state_btn[25] 		= new Fl_Button(555, 450, 60, 40, "");
	state_btn[26] 		= new Fl_Button(540, 515, 40, 50, "");
	state_btn[27] 		= new Fl_Button(575, 485, 55, 70, "");
	state_btn[28] 		= new Fl_Button(550, 560, 50, 60, "");
	state_btn[29] 		= new Fl_Button(605, 555, 30, 60, "");
	state_btn[30] 		= new Fl_Button(520, 630, 60, 25, "");
	state_btn[31] 		= new Fl_Button(580, 625, 65, 25, "");
	state_btn[32] 		= new Fl_Button(610, 405, 60, 80, "");
	state_btn[33] 		= new Fl_Button(660, 400, 50, 90, "");
	state_btn[34] 		= new Fl_Button(715, 400, 35, 55, "");
	state_btn[35] 		= new Fl_Button(670, 345, 100, 70, "");
	state_btn[36] 		= new Fl_Button(630, 335, 45, 65, "");
	state_btn[37] 		= new Fl_Button(625, 270, 35, 70, "");
	state_btn[38] 		= new Fl_Button(650, 205, 30, 130, "");
	state_btn[39] 		= new Fl_Button(690, 300, 60, 45, "");
	state_btn[40] 		= new Fl_Button(675, 260, 35, 60, "");
	state_btn[41] 		= new Fl_Button(670, 200, 35, 65, "");
	state_btn[42] 		= new Fl_Button(700, 195, 35, 85, "");
	state_btn[43] 		= new Fl_Button(745, 245, 50, 75, "");

	//visible also in second window
	bg_second_box 		= new Fl_Box(0, 10, 1200, 700);  	
	menu_bar 			= new Fl_Menu_Bar(0, 0, 1307, 30);
	audio_select 		= new Fl_Check_Button(145, 40, 70, 15, "Audio");
	change_view_btn 	= new Fl_Button(20, 40, 115, 30, "Change view");	
	exchange_btn		= new Fl_Button(20, 80, 145, 35, "EXCHANGE");

	for(int i=0; i<N_CONTRACTS; i++)
	{
		contract_btn[i] 	= new Fl_Button(contract_btn_x[i],  contract_btn_y[i], 30, 27, contract_btn_label[i].c_str());
	}
	for(int i=0; i<6; i++)
	{
		terr_card_btn[i] 	= new Fl_Button(5 + (i%3)*410,  300 + (int)(i/3)*370, 150, 27, "0");
		pri_exit_btn[2*i]	= new Fl_Button(5 + (i%3)*410,  340 + (int)(i/3)*370, 30, 27, "E");
		pri_exit_btn[2*i+1]	= new Fl_Button(45 + (i%3)*410,  340 + (int)(i/3)*370, 30, 27, "E");
	}
	

	main_screen->callback(window_cb);

	bg_main_box->image(bg_main_image);

	offer_btn->color((Fl_Color)3);
	offer_btn->labeltype(FL_ENGRAVED_LABEL);
	offer_btn->labelfont(14);
	offer_btn->hide();

	not_interested_btn->color((Fl_Color)3);
	not_interested_btn->labeltype(FL_ENGRAVED_LABEL);
	not_interested_btn->labelfont(14);
	not_interested_btn->hide();

	ok_btn->color((Fl_Color)3);
	ok_btn->labeltype(FL_ENGRAVED_LABEL);
	ok_btn->labelfont(14);
	ok_btn->hide();

	yes_btn->color((Fl_Color)3);
	yes_btn->labeltype(FL_ENGRAVED_LABEL);
	yes_btn->labelfont(14);
	yes_btn->hide();

	no_btn->color((Fl_Color)3);
	no_btn->labeltype(FL_ENGRAVED_LABEL);
	no_btn->labelfont(14);
	no_btn->hide();

	offer_input->box(FL_FLAT_BOX);
	offer_input->labeltype(FL_NO_LABEL);
	offer_input->textfont(14);
	offer_input->hide();

	info_display->buffer(info_text_buff);
	
	main_display->buffer(main_text_buff);

	army_display->buffer(army_text_buff);
	army_display->hide();

	for(int i=44; i>=0; i--)
	{
		state_btn[i]->box(FL_NO_BOX);
		state_btn[i]->labelsize(11);
		state_btn[i]->labelcolor(FL_BACKGROUND2_COLOR);
		state_btn[i]->clear();
	}

	for(int i=0; i<N_CONTRACTS - 6; i++)
	{
		for(int j=0; j<4; j++)
		{
			house_box[i*5 + j]->color((Fl_Color)63);
			house_box[i*5 + j]->box(FL_FLAT_BOX);
			house_box[i*5 + j]->hide();
		}
		house_box[i*5 + 4]->color((Fl_Color)88);
		house_box[i*5 + 4]->box(FL_FLAT_BOX);
		house_box[i*5 + 4]->hide();
	}
	bg_second_box->image(bg_second_image);
	bg_second_box->hide();

	change_view_btn->box(FL_THIN_UP_BOX);
	change_view_btn->color((Fl_Color)3);
	change_view_btn->labeltype(FL_ENGRAVED_LABEL);

	exchange_btn->color((Fl_Color)3);
	exchange_btn->labeltype(FL_ENGRAVED_LABEL);
	exchange_btn->labelfont(14);

	audio_select->down_box(FL_DOWN_BOX);
	audio_select->labeltype(FL_ENGRAVED_LABEL);
	audio_select->labelsize(17);
	audio_select->labelcolor((Fl_Color)3);
	audio_select->value(1);

	menu_bar->menu(menu_);
	
	for(int i=0; i<N_CONTRACTS; i++)
	{
		contract_btn[i]->color((Fl_Color)contract_btn_color[i]);
		contract_btn[i]->box(FL_UP_BOX);
		contract_btn[i]->hide();
	}	

	for(int i=0; i<6; i++)
	{
		terr_card_btn[i]->color((Fl_Color)198);
		terr_card_btn[i]->box(FL_UP_BOX);
		terr_card_btn[i]->hide();
		pri_exit_btn[2*i]->color((Fl_Color)78);
		pri_exit_btn[2*i]->box(FL_UP_BOX);
		pri_exit_btn[2*i]->hide();
		pri_exit_btn[2*i+1]->color((Fl_Color)78);
		pri_exit_btn[2*i+1]->box(FL_UP_BOX);
		pri_exit_btn[2*i+1]->hide();		
	}

	main_screen->end();
	main_screen->hide();

	intro_theme = load_wav("Soundtracks//Soundtrack_0.wav");
	main_theme  = load_wav("Soundtracks//Soundtrack_1.wav");
	

}

void risikopoly_client::draw_starting_screen()
{
	Fl_Double_Window* starting_screen 		= new Fl_Double_Window(1200, 705);
	Fl_JPEG_Image *bg_image 				= new Fl_JPEG_Image("Images//starting_screen.jpg");
	Fl_Box* bg_box 							= new Fl_Box(0, 20, 1200, 700);  
	Fl_Button* donate_btn 					= new Fl_Button(100, 360, 260, 75, "DONATE");
	Fl_Button* play_btn 					= new Fl_Button(100, 255, 260, 75, "PLAY");
	Fl_Input* user_input 					= new Fl_Input(500, 253, 200, 23, "USERNAME");
	Fl_Input* host_input 					= new Fl_Input(500, 289, 200, 25, "HOST IP");
	Fl_Button* connect_btn 					= new Fl_Button(465, 365, 215, 55, "CONNECT");
	Fl_Menu_Bar* menu_bar 					= new Fl_Menu_Bar(0, 0, 1307, 30);
	Fl_Box* wait_label 						= new Fl_Box(457, 309, 240, 47, "Please Wait");
	Fl_Check_Button* intro_audio_select 	= new Fl_Check_Button(30, 50, 70, 15, "Audio");

	starting_screen->callback(window_cb3);

	menu_[8].callback(help_callback);

	bg_box->image(bg_image);

	play_btn->box(FL_ENGRAVED_BOX);
	play_btn->color((Fl_Color)3);
	play_btn->labeltype(FL_SHADOW_LABEL);
	play_btn->labelfont(14);
	play_btn->labelsize(25); 
	
	donate_btn->box(FL_ENGRAVED_BOX);
	donate_btn->color((Fl_Color)3);
	donate_btn->labeltype(FL_SHADOW_LABEL);
	donate_btn->labelfont(14);
	donate_btn->labelsize(25);

	user_input->labeltype(FL_EMBOSSED_LABEL);
	user_input->labelsize(16);
	user_input->labelcolor((Fl_Color)3);
	user_input->hide();
	user_input->deactivate();

	host_input->labeltype(FL_EMBOSSED_LABEL);
	host_input->labelsize(16);
	host_input->labelcolor((Fl_Color)3);
	host_input->hide();
	host_input->deactivate();

	connect_btn->box(FL_ENGRAVED_BOX);
	connect_btn->color((Fl_Color)3);
	connect_btn->labeltype(FL_SHADOW_LABEL);
	connect_btn->labelfont(14);
	connect_btn->labelsize(25);
	connect_btn->hide();
	connect_btn->deactivate();

	wait_label->labeltype(FL_SHADOW_LABEL);
	wait_label->labelfont(14);
	wait_label->labelsize(53);
	wait_label->labelcolor((Fl_Color)3);
	wait_label->hide();

	intro_audio_select->down_box(FL_DOWN_BOX);
	intro_audio_select->labeltype(FL_ENGRAVED_LABEL);
	intro_audio_select->labelsize(17);
	intro_audio_select->labelcolor((Fl_Color)3);
	intro_audio_select->value(1);

	menu_bar->menu(menu_);

	starting_screen->end();
	starting_screen->show();

	play_sample(intro_theme, 255,128,1000, TRUE);

	int donation_money = 5;
	while(Fl::wait())
	{	
		if(play_btn->changed())
		{
			play_btn->clear();
			play_btn->hide();
			play_btn->deactivate();

			user_input->show();
			user_input->activate();
			//user_input->redraw();

			host_input->show();
			host_input->activate();
			//host_input->redraw();

			donate_btn->hide();
			donate_btn->deactivate();

			connect_btn->show();
			connect_btn->activate();	
			connect_btn->clear();	
		}
	
		if(intro_audio_select->changed())
		{
			//intro_audio_select->clear();
			//cout << "Ciaooo\n";
			if(intro_audio_select->value())
			{
				play_sample(intro_theme, 255,128,1000, TRUE);
				intro_audio_select->clear();
				intro_audio_select->value(1);
			}
			else
			{
				stop_sample(intro_theme);
				intro_audio_select->clear();
			}
		}

		if(donate_btn->changed())
		{
			donate_btn-> clear();
			while(1)
			{
				stringstream MSG;
				MSG << "Thank you very much for supporting RISIKOPOLY!\nDo you want to donate " << donation_money << " euro?";
				int choice = fl_choice("%s", "No", "Yes", 0, MSG.str().c_str());
				if(choice == 1)
				{
					fl_message("Accessing credit card information... Please wait...");
					sleep(5);
					fl_message("Your money transfer was successful! Thank you for your donation!!");
					break;
				}
				else if(donation_money < 1e9)
				{
					donation_money *= 2;
				}
			}
		}

		if(connect_btn->changed())
		{
			connect_btn->clear();
			string name = user_input->value();
			string host_address = host_input->value();
			if(name.size() == 0)
			{
				fl_message_title("Warning");
				fl_alert("Insert username");
			}
			else
			{
				if(connect_player(name, host_address) == -1)
				{
					fl_message_title("Warning");
					fl_alert("Host IP not found! \nInsert a valid host IP address");
				}
				else
				{
					user_input->hide();
					host_input->hide();
					wait_label->show();
					connect_btn->hide();
					Fl::flush();
					starting_screen->hide();
				
					get_initial_info();
					main_screen->show();
					stop_sample(intro_theme);
					play_sample(main_theme, 255,128,1000, TRUE);
					update_info_text();
					break;
				}
			}
		}
	}	
	
}


int risikopoly_client::get_money_auction()
{	
	string money = ""; 
	int money_return = 0;
	while(1)
	{
		check_buttons();
		Fl::wait(0.1);
		
		if(current_offer[1] == gamer_ID)
		{
			offer_input->deactivate();
			not_interested_btn->deactivate();
			offer_btn->deactivate();
		}
		else
		{
			offer_input->activate();
			not_interested_btn->activate();
			offer_btn->activate();
		}

		if(not_interested_btn->changed())
		{
			not_interested_btn->clear();
			offer_input->deactivate();
			not_interested_btn->deactivate();
			offer_btn->deactivate();
			return -1;
		}

		if(offer_btn->changed())
		{
			offer_btn->clear();
			money_return = atoi(offer_input->value());
			if(money_return > gamer[gamer_ID].money || money_return <= current_offer[0] || gamer_ID == current_offer[1])
			{
				fl_message_title("You'd like it");
				fl_alert("Make a proper offer");
			}
			else
			{
				break;
			}
		}

		synchronize();
		if (N_offerers < 2)
		{
			//current_offer[1] = gamer_ID;
			return current_offer[0];
		}
	}
	
	cout << "Offer : "<<money_return << "  citytanki\n";
	return money_return;
}


void risikopoly_client::decode_territory_name(int N_territory, int color)
{
	stringstream territory_name;
	territory_name.str("");
	territory_name << "Images//Territories//" << N_territory + 1 << "_";
	switch (color)
	{
		case BLACK:
			territory_name << "black.gif";
			break;
		case RED:
			territory_name << "red.gif";
			break;
		case BLUE:
			territory_name << "blue.gif";
			break;
		case GREEN:
			territory_name << "green.gif";
			break;
		case PURPLE:
			territory_name << "purple.gif";
			break;
		case YELLOW:
			territory_name << "yellow.gif";
			break;
		case WHITE:
			territory_name << "white.gif";
			break;
		case EMPTY:
			territory_name.str("");
			territory_name << "Images//Territories//empty.gif";
			break;
		default:
			territory_name.str("");
			territory_name << "Images//Territories//empty.gif";
			break;

	}
	
	territory_image[N_territory] = new Fl_GIF_Image(territory_name.str().c_str());
	territory_box[N_territory]->image(territory_image[N_territory]);
	territory_box[N_territory]-> redraw();
}


void risikopoly_client::decode_pawn_name(short int Player)
{
	stringstream pawn_name;
	pawn_name.str("");
	pawn_name << "Images//";
	switch (gamer[Player].color)
	{
		case BLACK:
			pawn_name << "Black.gif";
			break;
		case RED:
			pawn_name << "Red.gif";
			break;
		case BLUE:
			pawn_name << "Blue.gif";
			break;
		case GREEN:
			pawn_name << "Green.gif";
			break;
		case PURPLE:
			pawn_name << "Purple.gif";
			break;
		case YELLOW:
			pawn_name << "Yellow.gif";
			break;
		case WHITE:
			pawn_name << "White.gif";
			break;
		case EMPTY:
			pawn_name.str("");
			pawn_name << "Images//empty.gif";
			break;
		default:
			pawn_name.str("");
			pawn_name << "Images//Territories//empty.gif";
			break;

	}
	
	pawn_image[Player] = new Fl_GIF_Image(pawn_name.str().c_str());
	if(gamer[Player].position < 10)
	{
		pawn_box[Player]->position(820 + 3*Player - 53.5*gamer[Player].position, 665);
	}
	else if(gamer[Player].position < 20)
	{
		pawn_box[Player]->position(285 + 3*Player, 665 - 53*(gamer[Player].position%10));
	}
	else if(gamer[Player].position < 30)
	{
		pawn_box[Player]->position(285 + 3*Player + 53.5*(gamer[Player].position % 10), 135);
	}
	else
	{
		pawn_box[Player]->position(820 + 3*Player, 135 + 53*(gamer[Player].position%10));
	}
	pawn_box[Player]->image(pawn_image[Player]);
	pawn_box[Player]-> hide();
	pawn_box[Player]-> show();
}

void risikopoly_client::decode_contract_btn(int contract_number)
{
	if(window == 1)
	{
		int owner = contr[contract_number].owner;
		contract_btn[contract_number]->hide();		
		contract_btn[contract_number]-> redraw();
		if(owner < 6 && window == 1)
		{
			contract_btn[contract_number]->position(contract_btn_x[contract_number] + (owner%3)*410, contract_btn_y[contract_number] + (int)(owner/3)*370);
			contract_btn[contract_number]->show();
		}
		else 
		{
			contract_btn[contract_number]->hide();
		}
		contract_btn[contract_number]-> redraw();
	}
}

void risikopoly_client::decode_terr_card()
{
	for(int i=0; i<6; i++)
	{
		n_cannons[i] = 0; 
		n_pedestrians[i] = 0;
		n_horses[i] = 0;
		n_jollies[i] = 0;
	}
	for(int j=0; j<n_players; j++)
	{
		for(int i=0; i<N_TERRITORY_CARDS; i++)
		{
			if(terr_card[i].owner == j)
			{
				if(i < N_TERRITORIES/3)
				{
					n_cannons[j] ++;
				}
				else if(i < N_TERRITORIES/3*2)
				{
					n_pedestrians[j] ++;
				}
				else if(i < N_TERRITORIES)
				{
					n_horses[j] ++;
				}
				else
				{
					n_jollies[j] ++;
				}
			}
		}
	}
}

void risikopoly_client::decode_terr_card_btn()
{
	if(window == 0)
	{
		decode_terr_card();
		for(int i=0; i<n_players; i++)
		{
			terr_card_btn[i]->label("Territory cards");
			terr_card_btn[i]->show();
		}
		for(int i=0; i<6; i++)
		{		
			terr_card_btn[i]-> redraw();
		}
	}
	else
	{
		for(int i=0; i<6; i++)
		{
			terr_card_btn[i]-> hide();
			terr_card_btn[i]-> redraw();
		}
	}
}

void risikopoly_client::decode_pri_exit_btn()
{
	if(window == 0)
	{
		for(int i=0; i<6; i++)
		{		
			if(chance_card[15].owner == i)
			{
				pri_exit_btn[2*i]->show();
			}
			else
			{
				pri_exit_btn[2*i]->hide();
			}
			if(chest_card[11].owner == i)
			{
				pri_exit_btn[2*i+1]->show();
			}
			else
			{
				pri_exit_btn[2*i+1]->hide();
			}
		}
	}
	else
	{
		for(int i=0; i<12; i++)
		{
			pri_exit_btn[i]-> hide();
			pri_exit_btn[i]-> redraw();
		}
	}
}


void risikopoly_client::select_fighting_territories(bool FIGHT)
{
	for(int i=0; i<N_TERRITORIES + 1; i++)
	{
		if(state_btn[i]->changed())
		{
			if (i >= N_TERRITORIES)
			{
				state_btn[i]->clear();
				if(attacker < N_TERRITORIES)
				{
					decode_territory_name(attacker, gamer[terr[attacker].owner].color);
					attacker = 100;
					if(FIGHT)
					{
						fl_message("Choose the attacking territory");
					}
					else
					{
						fl_message("Select territory to donate armies");
					}
				}
				if(attacked < N_TERRITORIES)
				{
					decode_territory_name(attacked, gamer[terr[attacked].owner].color);
					attacked = 100;
				}
			}
			else if (attacker > N_TERRITORIES)
			{
				state_btn[i]->clear();
				if(i < N_TERRITORIES && terr[i].owner == gamer_ID && terr[i].n_offensive_armies > 1)
				{
					selected_state = i;
					attacker = i;
					stringstream army_text;
					army_text << terr[i].name <<"\nOff: " << terr[i].n_offensive_armies << "\nDef: "<< terr[i].n_defensive_armies<<endl;
					decode_territory_name(i, WHITE);
					army_display->show();
					army_text_buff->text(army_text.str().c_str());
					
					if(FIGHT)
					{
						fl_message("Choose the territory to attack");
					}
					else
					{
						fl_message("Select territory to receive armies");
					}
				}
				else
				{	
					army_display->hide();
					attacker = 100;
				}
				break;
				
			}
			else if (attacked > N_TERRITORIES)
			{
				state_btn[i]->clear();
				bool condition;
				if(FIGHT)
				{
					condition = terr[i].owner != gamer_ID;
				}
				else
				{
					condition = terr[i].owner == gamer_ID;
				}

				if(i < N_TERRITORIES && condition)
				{
					for(int j = 0; j < 8; j++)
					{
						if(j == 7)
						{
							return;
						}
						if(terr[attacker].border[j] == i)
						{
							break;
						}
					}
					selected_state = i;
					attacked = i;
					stringstream army_text;
					army_text << terr[i].name <<"\nOwner:  "<<gamer[terr[i].owner].name << "\nOff: " << terr[i].n_offensive_armies << "\nDef: "<< terr[i].n_defensive_armies<<endl;
					decode_territory_name(i, WHITE);
					army_display->show();
					army_text_buff->text(army_text.str().c_str());
				}
				break;
			}
		}
	}
}

short int risikopoly_client::select_armies(int atk)
{
	under_attack = false;
	int max_armies, n_armies;
	stringstream max_armies_string, message, summary_message;
	if(atk == 1)
	{
		max_armies = (terr[attacker].n_offensive_armies>3 ? 3 : (terr[attacker].n_offensive_armies - 1));
		summary_message << "Select number of attacker armies (MAX " << max_armies << ")\n";
	}
	else if(atk == 0)
	{
		max_armies = ((terr[attacked].n_offensive_armies + terr[attacked].n_defensive_armies)>3 ? 3 : (terr[attacked].n_offensive_armies + terr[attacked].n_defensive_armies));
		summary_message << "Select number of defensive armies (MAX " << max_armies << ")\n";
	}
	else 
	{
		max_armies = (terr[attacker].n_offensive_armies - 1);
		summary_message << "Select number of armies to move (MAX " << max_armies << ")\n";
	}
	fl_message("%s", summary_message.str().c_str());

	max_armies_string << max_armies;

	ok_btn->show();
	ok_btn->clear();
	offer_input->value(max_armies_string.str().c_str());
	offer_input->show();
	while(1)
	{
		synchronize();
		if(ok_btn->changed())
		{
			ok_btn->clear();
			n_armies = atoi(offer_input->value());
			if(atk == 1 && n_armies<=max_armies && n_armies > 0)
			{
				armies_atk = n_armies;
				ok_btn->hide();
				break;
			}
			else if(atk == 0 && n_armies<=max_armies && n_armies > 0)
			{
				armies_def = n_armies;
				ok_btn->hide();
				break;
			}
			else if(atk == 2 && n_armies<=max_armies && n_armies > 0)
			{
				ok_btn->hide();
				break;
			}
			
		}
		Fl::wait(0.1);
	}
	//synchronize();
	message.str("");
	if(atk == 2)
	{
		message << MOVETANK << "," << gamer_ID << "," << attacker << "," << attacked << "," << n_armies;
	}
	else
	{
		message << 	SELECT_ARMIES << "," << gamer_ID << "," << atk << "," << n_armies;
	}
	int n=1;

	ok_btn->hide();
	offer_input->hide();
	list_of_messages.push(message.str());
	send_all_messages();
}

void  risikopoly_client::draw_dice(short int die1, short int die2)
{
	stringstream die_name;
	if(die1 == 0)
	{
		for(int i=0; i<3; i++)
		{

			die_name.str("");
			die_name << "Images//Dice//";
			if(atk_value[i] == 0)
			{
				die_name << "empty.gif";
			}
			else
			{
				die_name << atk_value[i] << "_red.gif";
			}
			dice_image[i] = new Fl_GIF_Image(die_name.str().c_str());
			dice_box[i]->image(dice_image[i]);
			dice_box[i]-> redraw();

			die_name.str("");
			die_name << "Images//Dice//";
			if(def_value[i] == 0)
			{
				die_name << "empty.gif";
			}
			else
			{
				die_name.str("");
				die_name << "Images//Dice//";

				if(yellow_die[i])
				{
					die_name << def_value[i] << "_yellow.gif";
				}
				else
				{
					die_name << def_value[i] << "_blue.gif";
				}
			}
			dice_image[i+3] = new Fl_GIF_Image(die_name.str().c_str());
			dice_box[i+3]->image(dice_image[i+3]);
			dice_box[i+3]-> redraw();
		}
	}
	else
	{
		die_name.str("");
		die_name << "Images//Dice//";
		die_name << die1 << "_white.gif";
		dice_image[0] = new Fl_GIF_Image(die_name.str().c_str());
		dice_box[0]->image(dice_image[0]);
		dice_box[0]-> redraw();
		die_name.str("");
		die_name << "Images//Dice//";
		die_name << die2 << "_white.gif";
		dice_image[1] = new Fl_GIF_Image(die_name.str().c_str());
		dice_box[1]->image(dice_image[1]);
		dice_box[1]-> redraw();		
	}
	ok_btn->hide();
}

void risikopoly_client::conquer_territory()
{
	attack_successfull = true;
	int min_armies, n_armies;
	stringstream min_armies_string, message, summary_message;
	min_armies = armies_atk - armies_atk_lost;
	summary_message << "Select armies to move (minimum " << min_armies << ")\n";
	fl_message("%s", summary_message.str().c_str());

	min_armies_string << min_armies;

	ok_btn->show();
	ok_btn->clear();
	offer_input->value(min_armies_string.str().c_str());
	offer_input->show();
	while(1)
	{
		if(ok_btn->changed())
		{
			ok_btn->clear();
			n_armies = atoi(offer_input->value());
			if(n_armies >= min_armies && n_armies < terr[attacker].n_offensive_armies)
			{
				break;
			}
		}
		Fl::wait(0.1);
	}
	synchronize();
	message.str("");
	message << CONQUER_TERR << "," << gamer_ID << "," << n_armies;
	int n=1;
	list_of_messages.push(message.str());
	send_all_messages();
	ok_btn->hide();
	offer_input->hide();
}

void risikopoly_client::movement()
{
	stringstream message;
	if(gamer[gamer_ID].state == 2)
	{
		int choice = fl_choice("You are in Prison, do you want to pay 15000 ctk to exit?", "No", "Yes", 0);
		if(choice == 1)
		{
			message << PAY_FOR_PRISON << "," << gamer_ID;
			list_of_messages.push(message.str());
			send_all_messages();
			return;
		}
		else
		{
			fl_message("Press ok to trow dice \nand try to exit from prison");
		}
	}
	else
	{
		fl_message("Press ok for movement");
	}
	ok_btn->show();
	ok_btn->clear();
	short int dice_result[2], n = 1;
	while(1)
	{	
		synchronize();
		check_buttons();
		if(ok_btn->changed())
		{
			ok_btn->clear();
			dice_result[0] = rand() % 6 + 1;
			dice_result[1] = rand() % 6 + 1;
			
			break;
		}
		Fl::wait(0.1);
	}
	ok_btn->hide();
	message.str("");
	if(gamer[gamer_ID].state == 2)
	{
		message << PRISON << "," << gamer_ID << "," << dice_result[0] << "," << dice_result[1] << "," << turnsInPrison[gamer_ID];
	}
	else if(dice_result[0] == dice_result[1] && numberOfMove >= 3)
	{
		message << GOTOPRISON << "," << gamer_ID << "," << dice_result[0] << "," << dice_result[1];
	}
	else
	{
		message << MOVEMENT << "," << gamer_ID << "," << dice_result[0] << "," << dice_result[1];
	}
	cout << message.str() << endl;
	list_of_messages.push(message.str());

	while(list_of_messages.size()>0 || payment_needed == true)
	{
		synchronize();
		check_buttons();
		Fl::wait(0.1);
	}
	if(dice_result[0] == dice_result[1])
	{
		if(gamer[gamer_ID].state == 2)
		{
			turnsInPrison[gamer_ID] = 0;
			gamer[gamer_ID].state = 0;
		}
		else
		{
			numberOfMove ++;
		}
		if(numberOfMove < 3)
		{
			fl_message("Trow again");
			movement();
		}
		else
		{
			turnsInPrison[gamer_ID] = 0;
			gamer[gamer_ID].state = 2;
		}
	}
}

string risikopoly_client::make_offer(short int offerer, short int receiver, short int INSTR, bool visualize)
{
	stringstream return_value, max_string[4];

	int repetition = 0;
	if(INSTR == EXCHANGE)
	{
		repetition = 2;
	}
	else if(INSTR == OFFER)
	{
		repetition = 1;
	}

	for(int i=0; i<4; i++)
	{
		max_string[i] << "MAX: ";
	}
	max_string[0] << gamer[offerer].money;

	Fl_Double_Window 	*offert_screen;
	Fl_Box 				*bg_offert_box[2], *contract_btn_offer[2*N_CONTRACTS], *max_box[8];
	Fl_Button 			*make_offer_btn[2];
	Fl_Text_Display		*explanation_display[2];
	Fl_Text_Buffer		*offert_text_buff[2];
	Fl_Float_Input		*offert_input[8];
	Fl_Check_Button		*contract_check[2*N_CONTRACTS];

	offert_screen			= new Fl_Double_Window(700, 500);
	if(visualize)
	{
		make_offer_btn[0] 		= new Fl_Button(240, 400, 110, 60, "OK");
		make_offer_btn[1] 		= new Fl_Button(450, 400, 110, 60, "REFUSE");
	}
	else
	{
		make_offer_btn[0] 		= new Fl_Button(300, 400, 110, 60, "MAKE OFFER");
	}
	for(int j=0; j<repetition; j++)
	{	
		bg_offert_box[j]		= new Fl_Box(2 + 350*j, 75, 348, 320);
		explanation_display[j] 	= new Fl_Text_Display(5 + 350*j, 15, 335, 50);	
		offert_text_buff[j]		= new Fl_Text_Buffer();
		offert_input[0+4*j]		= new Fl_Float_Input(170 + 350*j, 86, 145, 34, "MONEY");
		offert_input[1+4*j]		= new Fl_Float_Input(170 + 350*j, 126, 145, 34, "HORSE CARDS");
		offert_input[2+4*j]	 	= new Fl_Float_Input(170 + 350*j, 166, 145, 34, "PEDESTRIAN CARDS");
		offert_input[3+4*j] 	= new Fl_Float_Input(170 + 350*j, 206, 145, 34, "CANNON CARDS");
		for(int i=0; i<N_CONTRACTS; i++)
		{
			contract_btn_offer[i+N_CONTRACTS*j] 	= new Fl_Box(contract_btn_x[i]+11 + 350*j,  contract_btn_y[i] + 150, 15, 15, contract_btn_label[i].c_str());
			contract_check[i+N_CONTRACTS*j]		= new Fl_Check_Button(contract_btn_x[i]-5 + 350*j, contract_btn_y[i] + 150, 70, 15);
		}
		for(int i=0; i<4; i++)
		{
			max_box[i+4*j]	= new Fl_Box(90 + 350*j, 115+40*i, 1, 1, max_string[i].str().c_str());
		}
	}

	offert_screen->callback(window_cb2);
	offert_screen->color((Fl_Color)219);

	if(visualize)
	{
		stringstream offert_text_string;
		offert_text_string << "OFFER FROM " << gamer[offerer].name;
		offert_text_buff[0]->insert(0, offert_text_string.str().c_str());
		if(INSTR == EXCHANGE)
		{
			offert_text_buff[1]->insert(0, "YOU");
		}
	}
	else
	{
		offert_text_buff[0]->insert(0, (INSTR == OFFER ? "YOU" : "OFFER"));
		if(INSTR == EXCHANGE)
		{
			offert_text_buff[1]->insert(0, gamer[receiver].name);
		}
	}
	make_offer_btn[0]->box(FL_SHADOW_BOX);
	make_offer_btn[0]->color((Fl_Color)3);
	make_offer_btn[0]->labeltype(FL_ENGRAVED_LABEL);
	if(visualize)
	{
		make_offer_btn[1]->box(FL_SHADOW_BOX);
		make_offer_btn[1]->color((Fl_Color)3);
		make_offer_btn[1]->labeltype(FL_ENGRAVED_LABEL);
	}
	for(int j=0; j<repetition; j++)
	{	
		bg_offert_box[j]->box(FL_UP_BOX);
		bg_offert_box[j]->color((Fl_Color)219);

		explanation_display[j]->labeltype(FL_NO_LABEL);
		explanation_display[j]->color((Fl_Color)219);
		explanation_display[j]->box(FL_NO_BOX);
		explanation_display[j]->textsize(20);
		explanation_display[j]->buffer(offert_text_buff[j]);

		for(int i=0; i<4; i++)
		{
			offert_input[i+4*j]->labeltype(FL_ENGRAVED_LABEL);
			max_box[i+4*j]->labelsize(10);
			max_box[i+4*j]->box(FL_NO_BOX);
			if(visualize)
			{
				stringstream value_offered;
				if(j==0)
				{
					value_offered << offered_items[i];
					offert_input[i]->value(value_offered.str().c_str());
				}
				else
				{
					value_offered << desidered_items[i];
					offert_input[i+4]->value(value_offered.str().c_str());
				}
				max_box[i+4*j]->hide();
			}
		}
	
		for(int i=0; i<N_CONTRACTS; i++)
		{
			contract_check[i+N_CONTRACTS*j]->down_box(FL_DOWN_BOX);
			contract_check[i+N_CONTRACTS*j]->labeltype(FL_NO_LABEL);
			if(visualize)
			{
				contract_check[i+N_CONTRACTS*j]->hide();
			}
			contract_btn_offer[i+N_CONTRACTS*j]->color((Fl_Color)contract_btn_color[i]);
			contract_btn_offer[i+N_CONTRACTS*j]->box(FL_UP_BOX);
			if(j==0)
			{
				if(visualize)
				{
					if(offered_items[i+4] != 1)
					{
						contract_btn_offer[i]->hide();
					}
				}
				else if(contr[i].owner == offerer)
				{
					contract_btn_offer[i]->show();
					contract_check[i]->show();
				}
				else
				{
					contract_btn_offer[i]->hide();
					contract_check[i]->hide();
				}
			}
			else
			{
				if(visualize)
				{
					if(desidered_items[i+4] != 1)
					{
						contract_btn_offer[i+N_CONTRACTS]->hide();
					}
				}
				else if(contr[i].owner == receiver)
				{
					contract_btn_offer[i+N_CONTRACTS]->show();
					contract_check[i+N_CONTRACTS]->show();
				}
				else
				{
					contract_btn_offer[i+N_CONTRACTS]->hide();
					contract_check[i+N_CONTRACTS]->hide();
				}
			}
		}
	}
	offert_screen->end();
	offert_screen->show();
	
	if(visualize)
	{
		return_value << DECIDE_EXCHANGE << "," << receiver << ",";
	}
	else
	{
		return_value << INSTR << "," << offerer << "," << receiver << ",";
	}
	while(1)
	{	
		synchronize();
		check_buttons();
		if(make_offer_btn[0]->changed())
		{
			make_offer_btn[0]->clear();
			if(visualize == true)
			{
				if(check_offer_validity())
				{
					return_value << 1 << ",";
				}
				else
				{
					return_value << 2 << ",";
				}
				break;
			}
			else if(INSTR == EXCHANGE)
			{
				if(atoi(offert_input[0]->value()) <= gamer[offerer].money && atoi(offert_input[4]->value()) <= gamer[receiver].money )
				{
					break;
				}
			}
			else
			{
				if(atoi(offert_input[0]->value()) <= gamer[offerer].money)
				{
					break;
				}
			}
		}
		if(visualize && make_offer_btn[1]->changed())
		{
			make_offer_btn[1]->clear();
			return_value << 0 << ",";
			break;
		}
		Fl::wait(0.1);
	}
	if(visualize == false)
	{
		for(int i = 0; i<4; i++)
		{
			return_value << atoi(offert_input[i]->value()) << ",";
		}
		for(int i=0; i<N_CONTRACTS; i++)
		{
			if(contract_check[i]->value())
			{
				return_value << i << ","; 
			}
		}
		return_value << END_OF_TRASMISSION << ",";
		if(INSTR == EXCHANGE)
		{
			for(int i = 0; i<4; i++)
			{
				return_value << atoi(offert_input[i+4]->value()) << ",";
			}
			for(int i=0; i<N_CONTRACTS; i++)
			{
				if(contract_check[i+N_CONTRACTS]->value())
				{
					return_value << i << ","; 
				}
			}
			return_value << END_OF_TRASMISSION << ",";
		}
	}

	delete offert_screen;
	return return_value.str();
}

short int risikopoly_client::choose_player()
{
	short int result, loop = 0;
	if(gamer_ID == 0)
	{
		loop = 1;
	}
	if(n_players == 1)
	{
		return 100;
	}

	while(1)
	{
		short int choice = fl_choice("Choose Player", "Exit", gamer[loop].name, "Others");
		if(choice == 0)
		{
			return 100;
		}
		else if(choice == 1)
		{
			result = loop;
			break;
		}
		else
		{
			loop ++;
			if(loop == gamer_ID)
			{
				loop ++;
			}
			if(loop >= (n_players))
			{
				loop = (0 == gamer_ID ? 1 : 0);
			}
		}
	}
	return result;
}

void risikopoly_client::update_info_text()
{
	count_player_terr();
	info_text.str("");
	for(int i = 0; i< n_players; i++)
	{
		info_text<<"**********************************\nPlayer "<<i<<": "<<gamer[i].name;
		if(i==gamer_ID)
		{
			info_text<<"  (you)";
		}
		if(gamer[i].state == 2)
		{
			info_text << " (prison)\n";
		}
		else
		{
			info_text<<endl;
		}
		info_text<<"Money :"<<gamer[i].money<<"\nTerritories: "<<gamer[i].n_territories<<endl<<endl;
	}
	info_text_buff->text(info_text.str().c_str());
}

void risikopoly_client::draw_houses()
{
	if(window == 1)
	{
		for(int i=0; i<((N_CONTRACTS-6)*5); i++)
		{
			house_box[i]->hide();
		}
	}
	else
	{	
		for(int i=0; i<(N_CONTRACTS - 6); i++)
		{
			for(int j=0; j<5; j++)
			{
				house_box[i * 5 + j]->hide();
			}
			if(contr[i].n_houses >= 5)
			{				
				house_box[i * 5 + 4]->show();
			}
			else
			{
				for(int j=0; j<contr[i].n_houses; j++)
				{
					house_box[i * 5 + j]->show();
				}
			}
		}
	}
	Fl::wait(0.1);
}

void risikopoly_client::payment(bool isContract, int price, int owner)
{
	stringstream message;	

	if(isContract)
	{
		int contr_number = position_contract[gamer[gamer_ID].position];
		price = contr[position_contract[gamer[gamer_ID].position]].value_house[contr[contr_number].n_houses];
		owner = contr[position_contract[gamer[gamer_ID].position]].owner;
		no_btn->label("Make offer");
		no_btn->show();
	}

	yes_btn->show();
	while(1)
	{
		if(price > gamer[gamer_ID].money)
		{
			yes_btn->label("Bankrupt");
		}
		else
		{
			yes_btn->label("Pay");
		}
		check_buttons();
		Fl::wait(0.1);
		synchronize();
		if(yes_btn -> changed())
		{
			yes_btn -> clear();
			if(price < gamer[gamer_ID].money)
			{
				message << PAYMENT << "," << gamer_ID << "," << owner << "," << price;
				list_of_messages.push(message.str()); 
				break;
			}
			
		}
		if(no_btn -> changed())
		{
			no_btn -> clear();
			try
			{
				list_of_messages.push(make_offer(gamer_ID, owner, OFFER, false));
			}
			catch(int i)
			{
				payment();
			}
			break;
		}
	}

	yes_btn->label("YES");
	yes_btn->hide();
	no_btn->label("NO");
	no_btn->hide();

}


void risikopoly_client::buy_tanks()
{
	int n_armies;
	ok_btn->show();
	offer_input->show();
	stringstream army_text, message;
	army_text.str("");
	army_text << "Select number of tanks to buy (1 tank = 15000 ctk)\n";
	army_text << "MAX = " << gamer[gamer_ID].money / 15000 << endl;
	army_display->show();
	army_text_buff->text(army_text.str().c_str());
	while(1)
	{
		check_buttons();
		synchronize();
		if(ok_btn->changed())
		{
			ok_btn->clear();
			n_armies = atoi(offer_input->value());
			if(n_armies * 15000 <= gamer[gamer_ID].money)
			{
				message << BUYTANK << "," << gamer_ID << "," << n_armies;
				list_of_messages.push(message.str());
				army_display->hide();
				ok_btn->hide();
				offer_input->hide();
				break;
			}
			else
			{
				fl_alert("You don't have enough money!");
			}
		}
		Fl::wait(0.1);
	}
	synchronize();
	army_display->hide();
	ok_btn->hide();
	offer_input->hide();

	send_all_messages();
}

void risikopoly_client::dispose_tanks(int n_tanks)
{
	disposing_tanks = true;
	int tanks[N_TERRITORIES];

	for(int i=0; i<N_TERRITORIES; i++)
	{
		tanks[i] = 0;
	}

	int n_tank_left = n_tanks;
	int n_tank_selected;
	stringstream army_text, message;
	while(n_tank_left > 0)
	{
		army_text.str("");
		army_text << "Select territory to dispose tanks (tanks left = " << n_tank_left << ")\n";
		fl_message("%s", army_text.str().c_str());

		while(1)
		{
			check_buttons();
			synchronize();
			for(int i=0; i<N_TERRITORIES + 1; i++)
			{
				if(state_btn[i]->changed())
				{			
					if(selected_state < N_TERRITORIES)
					{
						decode_territory_name(selected_state, terr[selected_state].owner < 100 ? gamer[terr[selected_state].owner].color : EMPTY);
					}
					selected_state = i;
					state_btn[i]->clear();
					if(selected_state < N_TERRITORIES && terr[selected_state].owner == gamer_ID)
					{
						ok_btn->show();
						offer_input->show();
						ok_btn->activate();
						decode_territory_name(i, WHITE);
						army_text.str("");
						army_text << terr[i].name <<"\nNumber of tanks: " << terr[i].n_offensive_armies << endl;
						army_text << "Decide number of tanks to dispose (";
						army_text << "MAX = " << n_tank_left << ")\n";
						army_display->show();
						army_text_buff->text(army_text.str().c_str());
					}
					else
					{
						ok_btn->deactivate();
						army_display->hide();
					}
					break;	
				}
			}

			if(ok_btn->changed())
			{
				ok_btn->clear();
				n_tank_selected = atoi(offer_input->value());
				if(n_tank_selected <= n_tank_left)
				{
					//message << BUYTANK << "," << gamer_ID << "," << n_armies;
					//list_of_messages.push(message.str());
					tanks[selected_state] += n_tank_selected;
					n_tank_left -= n_tank_selected;
					army_display->hide();
					ok_btn->hide();
					break;
				}
				else
				{
					fl_alert("You don't have enough tanks!");
				}
			}
			Fl::wait(0.1);
		}
	}
	disposing_tanks = false;

	army_display->hide();
	ok_btn->hide();
	offer_input->hide();	
	message << DISPOSETANK << "," << gamer_ID;
	for(int i=0 ; i < N_TERRITORIES; i++)
	{
		message << "," << tanks[i];
	}
	list_of_messages.push(message.str());

}

void risikopoly_client::remove_tanks(int n_tanks, bool onlyYou)
{
	disposing_tanks = true;
	int tanks[N_TERRITORIES], left_tanks[N_TERRITORIES];

	for(int i=0; i<N_TERRITORIES; i++)
	{
		tanks[i] = 0;
		left_tanks[i] = terr[i].n_offensive_armies - 1;
	}

	int n_tank_left = n_tanks;
	int n_tank_selected;
	stringstream army_text, message;
	while(n_tank_left > 0)
	{
		army_text.str("");
		army_text << "Select territory to remove tanks from (tanks left = " << n_tank_left << ")\n";
		fl_message("%s", army_text.str().c_str());

		while(1)
		{
			check_buttons();
			synchronize();
			for(int i=0; i<N_TERRITORIES + 1; i++)
			{
				if(state_btn[i]->changed())
				{			
					if(selected_state < N_TERRITORIES)
					{
						decode_territory_name(selected_state, terr[selected_state].owner < 100 ? gamer[terr[selected_state].owner].color : EMPTY);
					}
					selected_state = i;
					state_btn[i]->clear();
					if(selected_state < N_TERRITORIES && (terr[selected_state].owner == gamer_ID || !onlyYou))
					{
						ok_btn->show();
						offer_input->show();
						ok_btn->activate();
						decode_territory_name(i, WHITE);
						army_text.str("");
						army_text << terr[i].name <<"\nNumber of tanks: " << terr[i].n_offensive_armies << endl;
						army_text << "Decide number of tanks to remove (";
						army_text << "MAX = " << left_tanks[i] << ")\n";
						army_display->show();
						army_text_buff->text(army_text.str().c_str());
					}
					else
					{
						ok_btn->deactivate();
						army_display->hide();
					}
					break;	
				}
			}

			if(ok_btn->changed())
			{
				ok_btn->clear();
				n_tank_selected = atoi(offer_input->value());
				if(n_tank_selected <= left_tanks[selected_state])
				{
					//message << BUYTANK << "," << gamer_ID << "," << n_armies;
					//list_of_messages.push(message.str());
					tanks[selected_state] += n_tank_selected;
					left_tanks[selected_state] -= n_tank_selected;
					n_tank_left -= n_tank_selected;
					army_display->hide();
					ok_btn->hide();
					break;
				}
				else
				{
					fl_alert("There are not enough tanks to remove!");
				}
			}
			Fl::wait(0.1);
		}
	}
	disposing_tanks = false;

	army_display->hide();
	ok_btn->hide();
	offer_input->hide();	
	message << REMOVETANK << "," << gamer_ID;
	for(int i=0 ; i < N_TERRITORIES; i++)
	{
		message << "," << tanks[i];
	}
	list_of_messages.push(message.str());

}


void risikopoly_client::terr_card_view(int PLAYER)
{
	bool is_me = PLAYER == gamer_ID;
	int own_can_terr =0, own_ped_terr=0, own_horse_terr=0;
	vector<int> can_sel, ped_sel, horse_sel, jolly_sel; 
	stringstream info_message, message;
	decode_terr_card();
	if(is_me)
	{
		info_message << "Your territory cards: " << n_cannons[PLAYER] << " cannons, " << n_pedestrians[PLAYER] << " pedestrians, " << n_horses[PLAYER] << " horses, " << n_jollies[PLAYER] << " jollies.\n";
		for(int i=0; i<N_TERRITORY_CARDS; i++)
		{
			if(terr_card[i].owner == gamer_ID)
			{
				if(i < N_TERRITORIES/3)
				{
					info_message << terr[i].name << ": CANNON, owner : " << (terr[i].owner == gamer_ID ? "you" : gamer[terr[i].owner].name) << endl;
					if(terr[i].owner == gamer_ID)
					{
						own_can_terr ++;
						can_sel.push_back(i);
					}
					else
					{
						can_sel.insert(can_sel.begin(), i);
					}
				}
				else if(i < N_TERRITORIES/3*2)
				{
					info_message << terr[i].name << ": PEDESTRIAN, owner : " << (terr[i].owner == gamer_ID ? "you" : gamer[terr[i].owner].name) << endl;
					if(terr[i].owner == gamer_ID)
					{
						own_ped_terr ++;
						ped_sel.push_back(i);
					}
					else
					{
						ped_sel.insert(ped_sel.begin(), i);
					}
				}
				else if(i < N_TERRITORIES)
				{
					info_message << terr[i].name << ": HORSE, owner : " << (terr[i].owner == gamer_ID ? "you" : gamer[terr[i].owner].name) << endl;
					if(terr[i].owner == gamer_ID)
					{
						own_horse_terr ++;
						horse_sel.push_back(i);
					}
					else
					{
						horse_sel.insert(horse_sel.begin(), i);
					}
				}
				else
				{
					jolly_sel.push_back(i);
					info_message << "JOLLY\n";
				}
			}
		}
		if(player_turn == gamer_ID)
		{
			bool write_msg = true;
			if(n_cannons[PLAYER] >= 3)
			{
				write_msg = false;
				int win = 4 + (own_can_terr > 3 ? 3 : own_can_terr);
				info_message << "Do you want to change 3 cannons? (+" << win*15000 << " ctk)\n";
				int choice = fl_choice("%s", "No", "Yes", 0, info_message.str().c_str());
				if(choice == 1)
				{
					message.str("");
					message << CHANGE_TERR_CARDS << "," << gamer_ID << "," << win;
					for(int i=0; i<3; i++)
					{
						message << "," << can_sel.back();
						can_sel.pop_back();
					}
					list_of_messages.push(message.str());
					send_all_messages();
				}
			}
			if(n_pedestrians[PLAYER] >= 3)
			{
				write_msg = false;
				int win = 5 + (own_ped_terr > 3 ? 3 : own_ped_terr);
				info_message << "Do you want to change 3 pedestrians? (+" << win*15000 << " ctk)\n";
				int choice = fl_choice("%s", "No", "Yes", 0, info_message.str().c_str());
				if(choice == 1)
				{
					message.str("");
					message << CHANGE_TERR_CARDS << "," << gamer_ID << "," << win;
					for(int i=0; i<3; i++)
					{
						message << "," << ped_sel.back();
						ped_sel.pop_back();
					}
					list_of_messages.push(message.str());
					send_all_messages();
				}
			}
			if(n_horses[PLAYER] >= 3)
			{
				write_msg = false;
				int win = 6 + (own_horse_terr > 3 ? 3 : own_horse_terr);
				info_message << "Do you want to change 3 horses? (+" << win*15000 << " ctk)\n";
				int choice = fl_choice("%s", "No", "Yes", 0, info_message.str().c_str());
				if(choice == 1)
				{
					message.str("");
					message << CHANGE_TERR_CARDS << "," << gamer_ID << "," << win;
					for(int i=0; i<3; i++)
					{
						message << "," << horse_sel.back();
						horse_sel.pop_back();
					}

					list_of_messages.push(message.str());
					send_all_messages();
				}
			}
			if(n_cannons[PLAYER] >= 1 && n_pedestrians[PLAYER] >= 1 && n_horses[PLAYER] >= 1)
			{
				write_msg = false;
				int win = 8 + (own_can_terr > 1 ? 1 : own_can_terr) + (own_ped_terr > 1 ? 1 : own_ped_terr) + (own_horse_terr > 1 ? 1 : own_horse_terr);
				info_message << "Do you want to change 1 cannon, 1 pedestrian and 1 horse? (+" << win*15000 << " ctk)\n";
				int choice = fl_choice("%s", "No", "Yes", 0, info_message.str().c_str());
				if(choice == 1)
				{
					message.str("");
					message << CHANGE_TERR_CARDS << "," << gamer_ID << "," << win;
					message << "," << can_sel.back();
					can_sel.pop_back();
					message << "," << ped_sel.back();
					ped_sel.pop_back();
					message << "," << horse_sel.back();
					horse_sel.pop_back();
					list_of_messages.push(message.str());
					send_all_messages();
				}
			}
			if(n_jollies[PLAYER] >= 1 && n_cannons[PLAYER] >= 2)
			{
				write_msg = false;
				int win = 10 + (own_can_terr > 2 ? 2 : own_can_terr);
				info_message << "Do you want to change 1 jolly and 2 cannons? (+" << win*15000 << " ctk)\n";
				int choice = fl_choice("%s", "No", "Yes", 0, info_message.str().c_str());
				if(choice == 1)
				{
					message.str("");
					message << CHANGE_TERR_CARDS << "," << gamer_ID << "," << win;
					message << "," << can_sel.back();
					can_sel.pop_back();
					message << "," << can_sel.back();
					can_sel.pop_back();
					message << "," << jolly_sel.back();
					jolly_sel.pop_back();
					list_of_messages.push(message.str());
					send_all_messages();
				}
			}
			if(n_jollies[PLAYER] >= 1 && n_pedestrians[PLAYER] >= 2)
			{
				write_msg = false;
				int win = 10 + (own_ped_terr > 2 ? 2 : own_ped_terr);
				info_message << "Do you want to change 1 jolly and 2 pedestrians? (+" << win*15000 << " ctk)\n";
				int choice = fl_choice("%s", "No", "Yes", 0, info_message.str().c_str());
				if(choice == 1)
				{
					message.str("");
					message << CHANGE_TERR_CARDS << "," << gamer_ID << "," << win;
					message << "," << ped_sel.back();
					ped_sel.pop_back();
					message << "," << ped_sel.back();
					ped_sel.pop_back();
					message << "," << jolly_sel.back();
					jolly_sel.pop_back();
					list_of_messages.push(message.str());
					send_all_messages();
				}
			}
			if(n_jollies[PLAYER] >= 1 && n_horses[PLAYER] >= 2)
			{
				write_msg = false;
				int win = 10 + (own_horse_terr > 2 ? 2 : own_horse_terr);
				info_message << "Do you want to change 1 jolly and 2 horses? (+" << win*15000 << " ctk)\n";
				int choice = fl_choice("%s", "No", "Yes", 0, info_message.str().c_str());
				if(choice == 1)
				{
					message.str("");
					message << CHANGE_TERR_CARDS << "," << gamer_ID << "," << win;
					message << "," << horse_sel.back();
					horse_sel.pop_back();
					message << "," << horse_sel.back();
					horse_sel.pop_back();
					message << "," << jolly_sel.back();
					jolly_sel.pop_back();
					list_of_messages.push(message.str());
					send_all_messages();
				}
			}
			if(write_msg)
			{
				fl_message("%s", info_message.str().c_str());
			}
		}
		else
		{
			fl_message("%s", info_message.str().c_str());
		}
	}
	else
	{
		info_message << "Number of territory cards: " << n_cannons[PLAYER] + n_horses[PLAYER] + n_pedestrians[PLAYER] + n_jollies[PLAYER];
		fl_message("%s", info_message.str().c_str());
	}

}
