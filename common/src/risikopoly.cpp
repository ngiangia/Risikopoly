#include "risikopoly.h"
#include <algorithm>
#include <vector> 

risikopoly::risikopoly()
{
	n_players = 0;
	player_turn = 0;
	srand(time(NULL)); 
	for(int i = 0; i < 28; i++)
	{
		contr[i].owner = 100;
	}

 	n_contracts_per_player[0] 		= N_CONTRACTS;
 	n_contracts_per_player[1] 		= 7;
 	n_contracts_per_player[2] 		= 6;
 	n_contracts_per_player[3] 		= 5;
 	n_contracts_per_player[4] 		= 4;
 	n_contracts_per_player[5] 		= 3;
	

	for(int i=0; i<N_TERRITORY_CARDS; i++)
	{
		terr_card[i].owner = 100;
		terr_card[i].value = i;
	}
	for(int i=0; i<CHANCE_CARDS; i++)
	{
		chance_card[i].owner = 100;
		chance_card[i].value = i;
	}
	for(int i=0; i<CHEST_CARDS; i++)
	{
		chest_card[i].owner = 100;
		chest_card[i].value = i;
	}

	//assign name to territories
	string territories_name[44];
	territories_name[0] = "GROENLANDIA";
	territories_name[1] = "TERRITORI DEL NORD OVEST";
	territories_name[2] = "ALASKA";
	territories_name[3] = "ALBERTA";
	territories_name[4] = "STATI UNITI OCCIDENTALI";
	territories_name[5] = "AMERICA CENTRALE";
	territories_name[6] = "STATI UNITI ORIENTALI";
	territories_name[7] = "ONTARIO";
	territories_name[8] = "QUEBEC";
	territories_name[9] = "VENEZUELA";
	territories_name[10] = "PERU'";
	territories_name[11] = "ARGENTINA";
	territories_name[12] = "BRASILE";
	territories_name[13] = "INDONESIA";
	territories_name[14] = "NUOVA GUINEA";
	territories_name[15] = "AUSTRALIA OCCIDENTALE";
	territories_name[16] = "AUSTRALIA ORIENTALE";
	territories_name[17] = "ISLANDA";
	territories_name[18] = "GRAN BRETAGNA";
	territories_name[19] = "SCANDINAVIA";
	territories_name[20] = "UCRAINA";
	territories_name[21] = "EUROPA SETTENTRIONALE";
	territories_name[22] = "EUROPA OCCIDENTALE";
	territories_name[23] = "EUROPA MERIDIONALE";
	territories_name[24] = "AFRICA DEL NORD";
	territories_name[25] = "EGITTO";
	territories_name[26] = "CONGO";
	territories_name[27] = "AFRICA ORIENTALE";
	territories_name[28] = "AFRICA DEL SUD";
	territories_name[29] = "MADAGASCAR";
	territories_name[30] = "ANTARTIDE OCCIDENTALE";
	territories_name[31] = "ANTARTIDE ORIENTALE";
	territories_name[32] = "MEDIO ORIENTE";
	territories_name[33] = "INDIA";
	territories_name[34] = "SIAM";
	territories_name[35] = "CINA";
	territories_name[36] = "AFGHANISTAN";
	territories_name[37] = "URALI";
	territories_name[38] = "SIBERIA";
	territories_name[39] = "MONGOLIA";
	territories_name[40] = "CITA";
	territories_name[41] = "JACUZIA";
	territories_name[42] = "KAMCHATKA";
	territories_name[43] = "GIAPPONE";
	for(int i = 0; i < N_TERRITORIES; i++)
	{
		terr[i].owner = 100;
		terr[i].n_offensive_armies = 2;
		terr[i].n_defensive_armies = 0;
		for(int j = 0; j<7; j++)
		{
			terr[i].border[j] = 100;
		}
		terr[i].name = new char[territories_name[i].length() + 1];
		strcpy(terr[i].name, territories_name[i].c_str());
	}
	terr[0].border[0]  = 1;  terr[0].border[1]  = 7;  terr[0].border[1]  = 8;   terr[0].border[2] = 17;
	terr[1].border[0]  = 0;  terr[1].border[1]  = 2;  terr[1].border[2]  = 3;   terr[1].border[3] = 7;  
	terr[2].border[0]  = 1;  terr[2].border[1]  = 3;  terr[2].border[2]  = 42; 
	terr[3].border[0]  = 1;  terr[3].border[1]  = 2;  terr[3].border[2]  = 4;   terr[3].border[3] = 7;
	terr[4].border[0]  = 3;  terr[4].border[1]  = 5;  terr[4].border[2]  = 6;   terr[4].border[3] = 7;
	terr[5].border[0]  = 4;  terr[5].border[1]  = 5;  terr[5].border[2]  = 9;
	terr[6].border[0]  = 4;  terr[6].border[1]  = 5;  terr[6].border[2]  = 7;   terr[6].border[3] = 8;
	terr[7].border[0]  = 0;  terr[7].border[1]  = 1;  terr[7].border[2]  = 3;   terr[7].border[3] = 4; terr[7].border[4] = 6; terr[7].border[5] = 8;	
	terr[8].border[0]  = 0;  terr[8].border[1]  = 6;  terr[8].border[2]  = 7; 
	terr[9].border[0]  = 5;  terr[9].border[1]  = 10; terr[9].border[2]  = 12; 
	terr[10].border[0] = 9;  terr[10].border[1] = 11; terr[10].border[2] = 12; 
	terr[11].border[0] = 10; terr[11].border[1] = 12; terr[11].border[2] = 30; 
	terr[12].border[0] = 9;  terr[12].border[1] = 10; terr[12].border[2] = 11; terr[12].border[3] = 24;
	terr[13].border[0] = 14; terr[13].border[1] = 15; terr[13].border[2] = 34; 
	terr[14].border[0] = 13; terr[14].border[1] = 15; terr[14].border[2] = 16; 
	terr[15].border[0] = 13; terr[15].border[1] = 14; terr[15].border[2] = 16; terr[15].border[3] = 31;
	terr[16].border[0] = 14; terr[16].border[1] = 15; 
	terr[17].border[0] = 0;  terr[17].border[1] = 17; terr[17].border[2] = 18;
	terr[18].border[0] = 17; terr[18].border[1] = 19; terr[18].border[2] = 21; terr[18].border[3] = 22;
	terr[19].border[0] = 17; terr[19].border[1] = 18; terr[19].border[2] = 20; terr[19].border[3] = 21;
	terr[20].border[0] = 19; terr[20].border[1] = 21; terr[20].border[2] = 23; terr[20].border[3] = 32; terr[20].border[4] = 36; terr[20].border[5] = 37;
	terr[21].border[0] = 18; terr[21].border[1] = 19; terr[21].border[2] = 20; terr[21].border[3] = 22; terr[21].border[4] = 23;
	terr[22].border[0] = 18; terr[22].border[1] = 21; terr[22].border[2] = 23; terr[22].border[3] = 24; 
	terr[23].border[0] = 20; terr[23].border[1] = 21; terr[23].border[2] = 22; terr[23].border[3] = 24; terr[23].border[4] = 25; terr[23].border[5] = 32;
	terr[24].border[0] = 12; terr[24].border[1] = 22; terr[24].border[2] = 23; terr[24].border[3] = 25; terr[24].border[4] = 26; terr[24].border[5] = 27;
	terr[25].border[0] = 23; terr[25].border[1] = 24; terr[25].border[2] = 27; terr[25].border[3] = 32;
	terr[26].border[0] = 24; terr[26].border[1] = 27; terr[26].border[2] = 28; 
	terr[27].border[0] = 24; terr[27].border[1] = 25; terr[27].border[2] = 26; terr[27].border[3] = 28; terr[27].border[4] = 29;
	terr[28].border[0] = 26; terr[28].border[1] = 27; terr[28].border[2] = 29;
	terr[29].border[0] = 27; terr[29].border[1] = 28;  
	terr[30].border[0] = 11; terr[30].border[1] = 31;  
	terr[31].border[0] = 15; terr[31].border[1] = 30;  
	terr[32].border[0] = 20; terr[32].border[1] = 23; terr[32].border[2] = 25; terr[32].border[3] = 33; terr[32].border[4] = 35; terr[32].border[5] = 36;
	terr[33].border[0] = 32; terr[33].border[1] = 34; terr[33].border[2] = 35; 
	terr[34].border[0] = 13; terr[34].border[1] = 33; terr[34].border[2] = 35; 
	terr[35].border[0] = 32; terr[35].border[1] = 33; terr[35].border[2] = 34; terr[35].border[3] = 36; terr[35].border[4] = 37; terr[35].border[5] = 38; terr[35].border[5] = 39;
	terr[36].border[0] = 20; terr[36].border[1] = 32; terr[36].border[2] = 35; terr[36].border[3] = 37; 
	terr[37].border[0] = 20; terr[37].border[1] = 35; terr[37].border[2] = 36; terr[37].border[3] = 38;
	terr[38].border[0] = 35; terr[38].border[1] = 37; terr[38].border[2] = 39; terr[38].border[3] = 40; terr[38].border[4] = 41; 
	terr[39].border[0] = 35; terr[39].border[1] = 38; terr[39].border[2] = 40; terr[39].border[3] = 43;
	terr[40].border[0] = 38; terr[40].border[1] = 39; terr[40].border[2] = 41; terr[40].border[3] = 42;
	terr[41].border[0] = 38; terr[41].border[1] = 40; terr[41].border[2] = 42; 
	terr[42].border[0] = 2;  terr[42].border[1] = 39; terr[42].border[2] = 40; terr[42].border[3] = 41; terr[42].border[4] = 43;
	terr[43].border[0] = 39; terr[43].border[1] = 42; 
}


risikopoly::~risikopoly()
{
}
