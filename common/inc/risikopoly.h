#ifndef RISIKOPOLY_H
#define RISIKOPOLY_H

#include "structs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <ifaddrs.h>
#include <iostream>
#include "instructions.h"
#include <sstream>


using namespace std;

class risikopoly
{
	protected:
	player gamer[6];
	territory terr[N_TERRITORIES];
	contracts contr[N_CONTRACTS];
	card terr_card[N_TERRITORY_CARDS]; 
	card chance_card[CHANCE_CARDS];
	card chest_card[CHEST_CARDS];
	short int n_players;
	short int gamer_ID;
	short int n_contracts_per_player[6];
	
	//************************************* TCIP signals and functions
	struct sockaddr_in serv_addr, cli_addr;
	struct hostent *server;
	socklen_t clilen;
	char sock_buffer[256];

	//************************************* game signals and functions
	int current_offer[2], N_offerers;
	void init_game();
	short int player_turn;

	public:
	risikopoly();
	~risikopoly();

};

#endif
