#include <iostream>
#include <string>

struct player
{
	short int position;
	short int state;		//0 normal - 1 dead - 2 jail
	short int color;		//0 black - 1 red - 2 blue - 3 yellow - 4 green - 5 purple
	short int n_territories;
	int money;
	char *name;
	char *IP_addr;
};

struct territory
{
	char *name;
	short int owner;
	short int n_offensive_armies;
	short int n_defensive_armies;
	bool attack;		//true attack - false difence
	short int border[7];
};

struct contracts
{
	std::string name;
	short int owner;
	short int kind;		//color -- to be decided!!
	int price;
	int price_house;
	int value_house[6];
	short int n_houses;
	bool mortgage;
};

struct card
{
	short int owner;
	short int value;		//0-14 cannon - 15-29 pedestrian - 30-43 horse - 44-45 jolly 
};
