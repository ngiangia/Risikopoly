//message structure: N_ACTION , PLAYER_ID , DATA1 , DATA2 , ... , DATAN

#define BLACK	0
#define RED		1
#define BLUE	2
#define YELLOW	3
#define GREEN	4
#define PURPLE	5
#define WHITE	6
#define EMPTY  	7


#define N_TERRITORIES 		44
#define N_CONTINENTS		7
#define N_CONTRACTS			28
#define N_CONTRACTS_SERIES 	10
#define N_TERRITORY_CARDS	N_TERRITORIES+2
#define CHANCE_CARDS		18
#define CHEST_CARDS			17

#define END_OF_TRASMISSION	400

#define CLOSE_CB			0
#define LOSE_MONEY_CB		1


#define SYNC				0
#define CLOSE				1
#define INIT_INFO			2
#define ALL_CONTRACTS		3
#define ALL_TERRITORIES		4
#define ALLTERRCARDS		5
#define ALLCHANCECARDS		6
#define ALLCHESTCARDS		7
#define NEXT_TURN			8
#define LOAD				9
#define NOTHING				10
#define EXCHANGE			11
#define OFFER				12
#define DECIDE_EXCHANGE		13
#define BUY_HOUSE			14
#define PAY					15
#define PAYMENT				16
#define BUY_OR_AUCTION		17
#define BUY_CONTRACT		18
#define NEW_AUCTION			19
#define AUCTION				20
#define SELLTANK			21
#define BUYTANK				22
#define DISPOSETANK			23
#define MOVETANK			24
#define DICE_FIGHT			25
#define SELECT_ATTACKER		26
#define SELECT_ARMIES		27
#define CHANGE_TERR_CARDS	28
#define WIN_TERR_CARD		29
#define PRISON				30
#define GOTOPRISON			31
#define PRISON_EXIT			32
#define PAY_FOR_PRISON		33
#define CONQUER_TERR		34
#define MOVEMENT			35
#define PAY_TICKET			36
#define DRAW_CHANCE			37
#define REMOVETANK			38
